package com.tiger.it.solutions.migration.tool.utils;

import com.tiger.it.solutions.migration.tool.models.Config;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class ConfigLoader {

    private ConfigLoader() {
    }

    public static Config loadConfig(String jsonFilePath) throws IOException {
        String fileContent = Files.readString(Path.of(jsonFilePath), StandardCharsets.UTF_8);
        JSONObject object = new JSONObject(fileContent);
        return new Config(String.valueOf(object.getInt("version")), createDefault(object.getJSONObject("default")),
                createServerConfig(object.getJSONObject("server_config")), createFtp(object.getJSONObject("ftp")));
    }

    private static Config.Default createDefault(JSONObject object) {
        Config.LogLevel level;

        switch (object.getString("log_level").toUpperCase()) {
            case "WARN" -> level = Config.LogLevel.WARN;
            case "ERROR" -> level = Config.LogLevel.ERROR;
            case "DEBUG" -> level = Config.LogLevel.DEBUG;
            default -> level = Config.LogLevel.INFO;
        }

        String tempDirectory = object.getString("temporary_directory");

        return new Config.Default(object.getString("language"), object.getString("theme"),
                object.getBoolean("log"), level, object.getString("log_location"), tempDirectory);
    }

    private static Config.ServerConfig createServerConfig(JSONObject object) {
        return new Config.ServerConfig(object.getString("ip"), object.getBoolean("use_ssl"),
                object.getInt("port"));
    }

    private static Config.Ftp createFtp(JSONObject object) {
        return new Config.Ftp(object.getString("server"), object.getInt("port"),
                object.getString("username"), object.getString("password"));
    }
}
