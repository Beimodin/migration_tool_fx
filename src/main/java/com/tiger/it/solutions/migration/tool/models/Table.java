package com.tiger.it.solutions.migration.tool.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Table implements ModuleParser<Table> {

    private static final Logger LOGGER = Logger.getLogger(Table.class.getName());

    private String name;
    private TableColumn primaryColumn;
    private Queue<TableColumn> columns;

    public Table() {}

    public Table(String name, TableColumn primaryColumn, Queue<TableColumn> columns) {
        this.name = name;
        this.primaryColumn = primaryColumn;
        this.columns = columns;
    }

    public Table(String name, TableColumn primaryColumn) {
        this(name, primaryColumn, new ArrayDeque<>());
        this.columns.add(primaryColumn);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TableColumn getPrimaryColumn() {
        return primaryColumn;
    }

    public void setPrimaryColumn(TableColumn primaryColumn) {
        this.primaryColumn = primaryColumn;
    }

    public Queue<TableColumn> getColumns() {
        return columns;
    }

    public static boolean doesTableNameAlreadyExists(List<Table> tables, String tableName) {
        Optional<Table> op = tables.stream().filter(e -> e.name.equals(tableName)).findAny();
        return op.isPresent();
    }

    @Override
    public List<Table> parse(String json) {
        List<Table> tables = new ArrayList<>();

        try {
            JSONObject root = new JSONObject(json);
            JSONArray jsonTables = root.getJSONArray("output");

            for (int i = 0; i < jsonTables.length(); ++i) {
                JSONObject temp = jsonTables.getJSONObject(i);
                Queue<TableColumn> tableColumns = new ArrayDeque<>();
                TableColumn primaryColumn = null;

                JSONArray tempColumns = temp.getJSONArray("table_columns");
                for (int j = 0; j < tempColumns.length(); ++j) {
                    JSONObject tempColumn = tempColumns.getJSONObject(j);
                    TableColumn tableColumn = new TableColumn(tempColumn.getString("table_column_name"),
                            new Pair<>(tempColumn.getString("table_column_sql_type"), tempColumn.getString("table_column_rust_type")),
                            tempColumn.getBoolean("table_column_null_allowed"), tempColumn.getBoolean("table_column_primary_column"));
                    tableColumns.add(tableColumn);

                    if (tempColumn.getBoolean("table_column_primary_column"))
                        primaryColumn = tableColumn;
                    tableColumns.add(tableColumn);
                }
                tables.add(new Table(temp.getString("table_name"), primaryColumn, tableColumns));
            }
        } catch (JSONException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
            return new ArrayList<>();
        }
        return tables;
    }

    @Override
    public String toJson() {
        JSONObject object = new JSONObject();

        JSONObject temp = new JSONObject();
        String primaryColumn = this.primaryColumn.toJson();
        temp.put("name", this.name);
        temp.put("primary_column", primaryColumn);
        JSONArray columns = new JSONArray();

        this.columns.forEach(column -> columns.put(column.toJson()));
        temp.put("table_columns", columns);

        return object.toString();
    }

    @Override
    public String listToJson(List<Table> objects, boolean feedback) {
        JSONObject root = new JSONObject();
        JSONArray input = new JSONArray();

        objects.forEach(obj -> input.put(obj.toJson()));

        root.put("input", input);

        return root.toString();
    }
}
