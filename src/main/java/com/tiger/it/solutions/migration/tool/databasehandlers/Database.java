package com.tiger.it.solutions.migration.tool.databasehandlers;

import com.tiger.it.solutions.migration.tool.models.Module;
import com.tiger.it.solutions.migration.tool.models.*;

import java.util.List;
import java.util.Queue;
import java.util.Timer;

public interface Database {

    void startCheckSystem();

    List<Runnable> stopCheckSystem();

    List<Table> getTables();

    Queue<TableColumn> getTableColumns(String tableName);

    String removeTable(Table table);

    String addTable(Table table);

    String addTableColumn(Table table, TableColumn column);

    String removeTableColumn(TableColumn column);

    String changePrimaryColumn(Table table, TableColumn newPrimaryColumn, TableColumn oldPrimaryColumn);

    String changeIsNullAllowed(TableColumn column, boolean isNullAllowed);

    String changeIsPrimaryColumn(TableColumn column, boolean isPrimary);

    String updateTableColumn(TableColumn oldColumn, TableColumn newColumn);

    List<TableRelationship> getTableRelationships(List<Table> tables);

    String addTableRelationship(TableRelationship tableRelationship);

    String removeTableRelationship(TableRelationship tableRelationship);

    String updateTableRelationship(TableRelationship oldTableRelationship, TableRelationship newTableRelationship);


    List<Module> getModules();

    String addModule(Module module);

    String updateModule(Module oldModule, Module newModule);

    String removeModule(Module module);

    List<Workflow> getWorkflows();

    String addWorkflow(Workflow workflow);

    String updateWorkflow(Workflow oldWorkflow, Workflow newWorkflow);

    String removeWorkflow(Workflow workflow);

    List<Workflow.WorkflowNode> getWorkflowNodes();

    String addWorkflowNode(Workflow.WorkflowNode workflowNode);

    String updateWorkflow(Workflow.WorkflowNode oldWorkflowNode, Workflow.WorkflowNode newWorkflowNode);

    List<Workflow.WorkflowHistory> getWorkflowHistory();

    Workflow.WorkflowHistory getLatestWorkflow();

    Workflow.WorkflowHistory getWorkflow(Workflow.WorkflowHistory workflowHistory);

    String addWorkflowHistory(Workflow.WorkflowHistory workflowHistory);

    String updateWorkflowHistory(Workflow.WorkflowHistory oldWorkflowHistory, Workflow.WorkflowHistory newWorkflowHistory);

    String removeWorkflowHistory(Workflow.WorkflowHistory workflowHistory);

    List<LogType> getLogTypes();

    List<Workflow.EndSequence> getWorkflowEndSequence(Workflow workflow);

    String addEndSequence(Workflow.EndSequence endSequence, Workflow workflow);

    String updateEndSequence(Workflow.EndSequence oldEndSequence, Workflow.EndSequence newEndSequence, Workflow workflow);

    List<Workflow.Waiting> getWaitings();

    String addWaiting(Workflow.Waiting waiting, Workflow workflow);

    String updateWaiting(Workflow.Waiting oldWaiting, Workflow.Waiting newWaiting);

    String removeWaiting(Workflow.Waiting waiting);

    List<Variable> getVariables();

    List<Variable> getVariablesByWorkflow(Workflow workflow);

    String updateVariable(Variable oldVariable, Variable newVariable);

    String removeVariable(Variable variable);

    List<Datatype> getDatatypes();

    String updateDatatype(Datatype oldDatatype, Datatype newDatatype);

    String removeDatatype(Datatype datatype);

    List<Action> getActions();

    List<Api> getApis();

    String updateApi(Api oldApi, Api newApi);

    String removeApi(Api api);

    List<ApiType> getApiTypes();

    List<ApiType> getApiTypesByApi(String apiId);

    String updateApiType(ApiType oldApiType, ApiType newApiType);

    String removeApiType(ApiType apiType);

    String onClose();
}
