package com.tiger.it.solutions.migration.tool.utils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Hasher {

    private static final Logger LOGGER = Logger.getLogger(Hasher.class.getName());

    public static String hash(String... strings) {
        return hash(new StringBuilder(), strings);
    }

    private static String hash(StringBuilder s, String... strings) {
        if (strings.length == 0) {
            return s.toString();
        }
        String temp = strings[strings.length - 1];
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(temp.getBytes(StandardCharsets.UTF_8));
            s.append(Arrays.toString(hash));
            return hash(s, Arrays.copyOf(strings, strings.length - 1));
        } catch (NoSuchAlgorithmException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return null;
    }
}
