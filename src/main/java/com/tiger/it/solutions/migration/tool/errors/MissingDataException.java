package com.tiger.it.solutions.migration.tool.errors;

import java.io.IOException;

public class MissingDataException extends IOException {

    public MissingDataException(String message) {
        super(message);
    }
}
