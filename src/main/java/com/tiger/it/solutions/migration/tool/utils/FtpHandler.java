package com.tiger.it.solutions.migration.tool.utils;

import com.tiger.it.solutions.migration.tool.models.Config;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FtpHandler {

    private static final Logger LOGGER = Logger.getLogger(FtpHandler.class.getName());

    public final FTPClient client;

    private final String tempDirectory;

    public FtpHandler(Config.Ftp ftpConfig, Config.Default aDefault) throws IOException {
        this.client = new FTPClient();
        client.connect(ftpConfig.getUrl());
        int reply = client.getReplyCode();

        if (!FTPReply.isPositiveCompletion(reply)) {
            client.disconnect();
            LOGGER.log(Level.SEVERE, String.format("Exception while connecting to FTP server with code %d", reply));
            throw new IOException("Exception while connecting to FTP server");
        }

        client.login(ftpConfig.getUsername(), ftpConfig.getPassword());

        reply = client.getReplyCode();
        if (!FTPReply.isPositiveCompletion(reply)) {
            client.disconnect();
            LOGGER.log(Level.SEVERE, String.format("Exception while trying login to FTP server with code %d", reply));
            throw new IOException("Exception while trying login to FTP server");
        }

        tempDirectory = aDefault.getTemporaryDirectory();
    }

    public File downloadToDifferentDestination(String source, String destination) throws IOException {
        return downloadFile(source, destination);
    }

    public File downloadAsTemp(String source) throws IOException {
        String tempFileName = PathHandler.createPath(tempDirectory, Hasher.hash(""));
        return downloadFile(source, tempFileName);
    }

    private File downloadFile(String source, String destination) throws IOException {
        FileOutputStream out = new FileOutputStream(destination);
        client.retrieveFile(source, out);
        return new File(destination);
    }

    public String uploadFile(File file, String path) {
        try {
            client.storeFile(path, new FileInputStream(file));
            return "Successfully uploaded file";
        } catch (IOException ex) {
            ex.printStackTrace();
            return String.format("Failed to upload file, %s", ex.getMessage());
        }
    }

    private void close() throws IOException {
        client.disconnect();
    }
}
