package com.tiger.it.solutions.migration.tool.utils;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class HttpHandler {

    private final String completeUrl;

    public HttpHandler(String host, int port) {
        completeUrl = String.format("%s:%d", host, port);
    }

    public HttpHandler(String host) {
        completeUrl = String.format("%s", host);
    }

    public String executeRequest(HttpType httpType, String url) throws IOException,
            ExecutionException, InterruptedException {
        return executeRequest(httpType, url, "");
    }

    public String executeRequest(HttpType httpType, String url, String body) throws
            IllegalArgumentException, ExecutionException, InterruptedException, IOException {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = null;
        String completedUrl = createUrl(url);

        switch (httpType) {
            case GET -> request = HttpRequest.newBuilder().uri(URI.create(completedUrl)).GET().build();
            case PUT -> {
                if (body.isEmpty()) {
                    request = HttpRequest.newBuilder().uri(URI.create(completedUrl))
                            .PUT(HttpRequest.BodyPublishers.noBody()).build();
                } else {
                    request = HttpRequest.newBuilder().uri(URI.create(completedUrl))
                            .PUT(HttpRequest.BodyPublishers.ofString(body)).build();
                }
            }
            case POST -> {
                if (body.isEmpty()) {
                    request = HttpRequest.newBuilder().uri(URI.create(completedUrl))
                            .POST(HttpRequest.BodyPublishers.noBody()).build();
                } else {
                    request = HttpRequest.newBuilder().uri(URI.create(completedUrl))
                            .POST(HttpRequest.BodyPublishers.ofString(body)).build();
                }
            }
            case DELETE -> request = HttpRequest.newBuilder().uri(URI.create(completedUrl)).DELETE().build();
        }

        if (request == null)
            throw new IOException("Request is null");

        CompletableFuture<HttpResponse<String>> futureResponse = client.sendAsync(request,
                HttpResponse.BodyHandlers.ofString());
        HttpResponse<String> response = futureResponse.get();

        switch (response.statusCode()) {
            case 200 -> { return response.body(); }
            default -> {
                throw new IllegalArgumentException(String.format("Request failed with %d", response.statusCode()));
            }
        }
    }

    public enum HttpType {
        POST, GET, PUT, DELETE
    }

    private String createUrl(String url) {
        return String.format("%s/%s", completeUrl, url);
    }
}
