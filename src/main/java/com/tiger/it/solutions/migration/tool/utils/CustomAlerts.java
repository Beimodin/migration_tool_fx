package com.tiger.it.solutions.migration.tool.utils;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

public class CustomAlerts {

    private CustomAlerts() {
    }

    public static boolean confirmation(String title, String message, Alert.AlertType alertType) {
        Alert alert = new Alert(alertType);
        alert.setHeaderText(title);
        alert.setContentText(message);

        ButtonType yes = new ButtonType("Yes");
        ButtonType no = new ButtonType("No");
        alert.getButtonTypes().setAll(no, yes);
        Optional<ButtonType> result = alert.showAndWait();
        return result.get() == yes;
    }

    public static void warning(String title, String message) {
        CustomAlerts.alert(Alert.AlertType.WARNING, title, message);
    }

    public static void alert(Alert.AlertType type, String title, String message) {
        Alert alert = new Alert(type);
        alert.setHeaderText(title);
        alert.setContentText(message);
        ButtonType ok = new ButtonType("Ok");
        alert.getButtonTypes().setAll(ok);
        alert.showAndWait();
    }
}
