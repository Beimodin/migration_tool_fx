package com.tiger.it.solutions.migration.tool.views.main;

import com.tiger.it.solutions.migration.tool.databasehandlers.Database;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuBar;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

public class MainController implements Initializable {

    @FXML
    private AnchorPane mainAnchorPane;

    @FXML
    private MenuBar menuBarMainView;

    @FXML
    private TabPane mainTabPane;

    @FXML
    private Tab mainTabSqlWorkbench;

    @FXML
    private Tab mainTabModules;

    @FXML
    private Tab mainTabWorkflow;

    @FXML
    private Database database;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Preferences preferences = Preferences.userRoot().node(this.getClass().getName());
        Platform.runLater(() -> {
            MainControllerMethods.onTabChange(mainAnchorPane, mainTabPane, menuBarMainView);
            MainControllerMethods.loadSqlWorkbench(mainTabSqlWorkbench, database);
            MainControllerMethods.loadModuleTab(mainTabModules, database);
            MainControllerMethods.loadWorkflowTab(mainTabWorkflow, database);
        });
    }

    public void setDatabase(Database database) {
        this.database = database;
    }
}
