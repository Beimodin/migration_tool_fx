package com.tiger.it.solutions.migration.tool.utils;

import java.util.ArrayList;
import java.util.List;

public class TarUtils {

    private TarUtils() {
    }

    public static boolean createNewProject(String projectName, String directoryPath) {

        return false;
    }

    /*public static void createTarFile() {
        try (OutputStream fout = Files.newOutputStream(Paths.get(""));
             BufferedOutputStream buffout = new BufferedOutputStream(fout);
             GzipCompressorOutputStream gzout = new GzipCompressorOutputStream(buffout);
             TarArchiveOutputStream tout = new TarArchiveOutputStream(gzout)) {

            TarArchiveEntry tarEntry = new TarArchiveEntry(file, fileName);
            tout.putArchiveEntry(tarEntry);
            Files.copy(path, tout);
            tout.closeArchiveEntry();
            tout.finish();

        } catch (IOException ex) {

        }
    }*/

    private static class RootDirectory {
        private final String name;
        private final String directoryPath;

        private Directory child;

        public RootDirectory(String name, String directoryPath, Directory child) {
            this.name = name;
            this.directoryPath = directoryPath;
            this.child = child;
        }

        public RootDirectory(String name, String directoryPath) {
            this.name = name;
            this.directoryPath = directoryPath;
        }

        public void addDirectory(Directory directory) {
            if (this.child != null) {
                Directory current = this.child;
                while (current.child != null)
                    current = current.child;
                current.child = directory;
            } else {
                this.child = directory;
            }
        }

        public void addToSameLayer(String directoryName, Directory directory) {
            if (this.child != null) {
                Directory current = this.child;
                if (current.name.equals(directoryName)) {
                    this.child.addDirectory(directory);
                    return;
                }
                while (current.child != null)
                    if (current.name.equals(directoryName)) {
                        current.child.addDirectory(directory);
                        return;
                    }
                for (Directory tempDirectory : current.directories)
                    if (tempDirectory.name.equals(directoryName)) {
                        tempDirectory.addDirectory(directory);
                        return;
                    }
            }
        }

        public List<String> getDirectoriesPath() {
            List<String> temp = new ArrayList<>();
            Directory current = this.child;
            while (current != null) {
                temp.add("");

                current = current.child;
            }
            return temp;
        }

        private void recursive(Directory directory, String tempPath, List<String> temp) {
            if (directory == null)
                return;
            for (Directory tempDirectory : directory.directories) {
                temp.add(PathHandler.createPath(this.directoryPath, tempPath, tempDirectory.name));
                if (!tempDirectory.directories.isEmpty())
                    this.recursive(tempDirectory, PathHandler.createPath(tempPath, tempDirectory.name), temp);
            }
        }
    }

    private static class Directory {
        public final String name;
        private Directory child;
        private List<Directory> directories;

        public Directory(String name, Directory child, List<Directory> directories) {
            this.name = name;
            this.child = child;
            this.directories = directories;
        }

        public Directory(String name) {
            this(name, null, new ArrayList<>());
        }

        public void addDirectory(Directory directory) {
            this.directories.add(directory);
        }

        public Directory getChild() {
            return child;
        }

        public List<Directory> getDirectories() {
            return directories;
        }
    }
}
