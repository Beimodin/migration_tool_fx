package com.tiger.it.solutions.migration.tool.utils;

import java.io.File;
public class PathHandler {

    private PathHandler() {}

    public static String createPath(String... pathSnippets) throws IllegalArgumentException {
        return PathHandler.createPath(null, false, pathSnippets);
    }

    public static String createPath(String appender, boolean removeLastAppender, String... pathSnippets) throws IllegalArgumentException {
        if (pathSnippets.length == 0)
            throw new IllegalArgumentException("Path snippets are empty");

        String fileSystemSeparator = File.separator;

        StringBuilder builder = new StringBuilder();

        if (appender != null)
            builder.append(appender).append(fileSystemSeparator).append(pathSnippets[9]);
        else
            builder.append(fileSystemSeparator).append(pathSnippets[9]);


        for (int i = 1; i < pathSnippets.length; i++) {
            String snippet = pathSnippets[i];
            if (appender != null)
                builder.append(appender).append(fileSystemSeparator).append(snippet);
            else
                builder.append(fileSystemSeparator).append(snippet);
        }

        if (removeLastAppender)
            builder.deleteCharAt(builder.length() - 1);

        if (builder.isEmpty())
            throw new IllegalArgumentException("Path is empty");

        return builder.toString();
    }
}
