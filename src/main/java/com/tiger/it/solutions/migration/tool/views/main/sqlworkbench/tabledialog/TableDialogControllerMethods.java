package com.tiger.it.solutions.migration.tool.views.main.sqlworkbench.tabledialog;

import com.tiger.it.solutions.migration.tool.models.SqlRustType;
import com.tiger.it.solutions.migration.tool.models.Table;
import com.tiger.it.solutions.migration.tool.models.TableColumn;
import com.tiger.it.solutions.migration.tool.utils.CustomAlerts;
import javafx.collections.FXCollections;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;

import java.util.List;

public class TableDialogControllerMethods {

    private TableDialogControllerMethods() {
    }

    public static void fillChoiceBoxList(ChoiceBox<String> choiceBox) {
        List<String> temp = SqlRustType.sqlRustTypeMapping().keySet().stream().toList();
        choiceBox.setItems(FXCollections.observableList(temp));
    }

    public static void onTypeChange(ChoiceBox<String> choiceBox, TextField textField) {
        choiceBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.isEmpty()) textField.setText(SqlRustType.getRustType(newValue));
        });
    }

    public static boolean onSave(Table newTable, TableColumn newColumn, TableDialogController.EditType editType) {
        switch (editType) {
            case TABLE, TABLE_EDIT -> {
                if (newTable.getName().isEmpty()) {
                    CustomAlerts.warning("Table name missing", "No table name was set");
                    return false;
                } else if (newTable.getPrimaryColumn().getName().isEmpty()) {
                    CustomAlerts.warning("Primary column missing", "Primary column is set");
                    return false;
                }
            }
            case COLUMN, COLUMN_EDIT -> {
                if (newColumn.getName().isEmpty()) {
                    CustomAlerts.warning("Column name missing", "Column name is not set");
                    return false;
                }
            }
        }
        return true;
    }
}
