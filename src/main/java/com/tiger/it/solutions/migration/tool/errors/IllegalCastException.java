package com.tiger.it.solutions.migration.tool.errors;

public class IllegalCastException extends Exception {

    public IllegalCastException(String message) {
        super(message);
    }
}
