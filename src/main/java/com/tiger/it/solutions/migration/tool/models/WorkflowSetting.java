package com.tiger.it.solutions.migration.tool.models;

import java.util.List;

public class WorkflowSetting {

    public final String id;
    private String name;

    public final List<WorkflowSettingValueType> allowedTypes;

    private WorkflowSettingValueType value;

    private WorkflowSettingValueType defaultValue;

    private boolean globalInherited;

    private boolean overwriteGlobalSetting;

    public WorkflowSetting(String id, String name, List<WorkflowSettingValueType> allowedTypes, WorkflowSettingValueType value,
                           WorkflowSettingValueType defaultValue, boolean globalInherited, boolean overwriteGlobalSetting) {
        this.id = id;
        this.name = name;
        this.allowedTypes = allowedTypes;
        this.value = value;
        this.defaultValue = defaultValue;
        this.globalInherited = globalInherited;
        this.overwriteGlobalSetting = overwriteGlobalSetting;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public WorkflowSettingValueType getValue() {
        return value;
    }

    public void setValue(WorkflowSettingValueType value) {
        this.value = value;
    }

    public WorkflowSettingValueType getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(WorkflowSettingValueType defaultValue) {
        this.defaultValue = defaultValue;
    }

    public boolean isGlobalInherited() {
        return globalInherited;
    }

    public void setGlobalInherited(boolean globalInherited) {
        this.globalInherited = globalInherited;
    }

    public boolean isOverwriteGlobalSetting() {
        return overwriteGlobalSetting;
    }

    public void setOverwriteGlobalSetting(boolean overwriteGlobalSetting) {
        this.overwriteGlobalSetting = overwriteGlobalSetting;
    }

    public static class WorkflowSettingValueType<T> {

        public final String id;

        public final String type;
        private T value;

        public WorkflowSettingValueType(String id, String type, T value) {
            this.id = id;
            this.type = type;
            this.value = value;
        }

        public T getValue() {
            return value;
        }

        public String getValueAsString() {
            return (String) value;
        }

        public Integer getValueAsInteger() {
            return (Integer) value;
        }

        public Double getValueAsDouble() {
            return (Double) value;
        }

        public Boolean getValueAsBoolean() {
            return (Boolean) value;
        }

        public Character getValueAsChar() {
            return (Character) value;
        }
    }
}