package com.tiger.it.solutions.migration.tool.models;

import com.tiger.it.solutions.migration.tool.errors.MissingDataException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Config {

    private static final Logger LOGGER = Logger.getLogger(Config.class.getName());

    public final String version;
    private Default aDefault;
    private ServerConfig serverConfig;

    private Ftp ftp;

    public Config(String version, Default aDefault, ServerConfig serverConfig, Ftp ftp) {
        this.version = version;
        this.aDefault = aDefault;
        this.serverConfig = serverConfig;
        this.ftp = ftp;
    }

    public String getVersion() {
        return version;
    }

    public Default getaDefault() {
        return aDefault;
    }

    public void setaDefault(Default aDefault) {
        this.aDefault = aDefault;
    }

    public ServerConfig getServerConfig() {
        return serverConfig;
    }

    public void setServerConfig(ServerConfig serverConfig) {
        this.serverConfig = serverConfig;
    }

    public Ftp getFtp() {
        return ftp;
    }

    public void setFtp(Ftp ftp) {
        this.ftp = ftp;
    }

    public static class Default {
        private String language;
        private String theme;
        private boolean log;
        private LogLevel logLevel;
        private String logLocation;

        private String temporaryDirectory;

        public Default(String language, String theme, boolean log, LogLevel logLevel, String logLocation,
                       String temporaryDirectory) {
            this.language = language;
            this.theme = theme;
            this.log = log;
            this.logLevel = logLevel;
            this.logLocation = logLocation;
            this.temporaryDirectory = temporaryDirectory;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getTheme() {
            return theme;
        }

        public void setTheme(String theme) {
            this.theme = theme;
        }

        public boolean isLog() {
            return log;
        }

        public void setLog(boolean log) {
            this.log = log;
        }

        public LogLevel getLogLevel() {
            return logLevel;
        }

        public void setLogLevel(LogLevel logLevel) {
            this.logLevel = logLevel;
        }

        public String getLogLocation() {
            return logLocation;
        }

        public void setLogLocation(String logLocation) {
            this.logLocation = logLocation;
        }

        public String getTemporaryDirectory() throws MissingDataException {
            if (temporaryDirectory == null) {
                LOGGER.log(Level.INFO,"Temporary directory is not set. Trying to use the system temporary directory");
                temporaryDirectory = System.getProperty("java.io.tmpdir");
            }

            Path temp = Path.of(temporaryDirectory);

            if (!Files.exists(temp) || !Files.isDirectory(temp)) {
                LOGGER.log(Level.SEVERE,"The specified directory does not exists or is not a directory");
                throw new MissingDataException("Users or system temporary directory cannot be accessed");
            }

            return temporaryDirectory;
        }

        public void setTemporaryDirectory(String temporaryDirectory) {
            this.temporaryDirectory = temporaryDirectory;
        }
    }

    public static class ServerConfig {
        private String ip;
        private boolean useSsl;
        private int port;

        private String url;

        public ServerConfig(String ip, boolean useSsl, int port) {
            this.ip = ip;
            this.useSsl = useSsl;
            this.port = port;
            this.url = createUrl();
        }

        private String createUrl() {
            String ssl = useSsl ? "https://" : "http://";
            return port > 0 ? String.format("%s%s:%d", ssl, ip, port) :
                    String.format("%s%s", ssl, ip);
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public boolean isUseSsl() {
            return useSsl;
        }

        public void setUseSsl(boolean useSsl) {
            this.useSsl = useSsl;
        }

        public int getPort() {
            return port;
        }

        public void setPort(int port) {
            this.port = port;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

    public static class Ftp {
        private String server;
        private int port;
        private String username;
        private String password;
        private String url;

        public Ftp(String server, int port, String username, String password) {
            this.server = server;
            this.port = port;
            this.username = username;
            this.password = password;
            this.url = createUrl();
        }

        private String createUrl() {
            return port <= 0 ? String.format("https://%s:%d", server, port) :
                    String.format("https://%s", server);
        }

        public String getServer() {
            return server;
        }

        public void setServer(String server) {
            this.server = server;
        }

        public int getPort() {
            return port;
        }

        public void setPort(int port) {
            this.port = port;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getUrl() {
            return url;
        }

        public void reloadUrl() {

        }
    }

    public enum LogLevel {
        DEBUG, INFO, ERROR, WARN
    }
}
