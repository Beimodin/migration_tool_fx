package com.tiger.it.solutions.migration.tool.views.main.sqlworkbench.tabledialog;

import com.tiger.it.solutions.migration.tool.models.Pair;
import com.tiger.it.solutions.migration.tool.models.SqlRustType;
import com.tiger.it.solutions.migration.tool.models.Table;
import com.tiger.it.solutions.migration.tool.models.TableColumn;
import com.tiger.it.solutions.migration.tool.utils.CustomAlerts;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.net.URL;
import java.util.List;
import java.util.Queue;
import java.util.ResourceBundle;

public class TableDialogController implements Initializable {
    @FXML
    private CheckBox checkBoxNullAllowed;

    @FXML
    private CheckBox checkBoxPrimary;

    @FXML
    private TextField textFieldTableName;

    @FXML
    private TextField textFieldPrimaryColumnName;

    @FXML
    private TextField textFieldRustType;

    @FXML
    private ChoiceBox<String> choiceBoxSqlType;

    @FXML
    private Button buttonCancel;

    @FXML
    private Button buttonSave;

    @FXML
    private Label labelTableName;

    @FXML
    private Label labelColumnName;

    @FXML
    private List<Table> tables;

    @FXML
    private Queue<TableColumn> tableColumns;

    @FXML
    private Table table;

    @FXML
    private TableColumn tableColumn;

    @FXML
    private EditType editType;

    private Table newTable;
    private TableColumn newColumn;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        textFieldRustType.setEditable(false);
        TableDialogControllerMethods.fillChoiceBoxList(choiceBoxSqlType);
        TableDialogControllerMethods.onTypeChange(choiceBoxSqlType, textFieldRustType);
        Platform.runLater(() -> buttonSave.setOnAction(e -> {
            if (choiceBoxSqlType.getSelectionModel().isEmpty()) {
                CustomAlerts.warning("SQL Type not set", "Select SQL type");
            } else {
                switch (editType) {
                    case TABLE -> {
                        if (Table.doesTableNameAlreadyExists(tables, textFieldTableName.getText())) {
                            CustomAlerts.warning("Table name already exists", "Choose a different table name");
                        }
                        newTable = new Table(textFieldTableName.getText(), new TableColumn(
                                textFieldPrimaryColumnName.getText(), new Pair<>(choiceBoxSqlType.getSelectionModel()
                                .getSelectedItem(), textFieldRustType.getText()), checkBoxNullAllowed.isSelected(),
                                checkBoxPrimary.isSelected()));
                    }
                    case COLUMN -> {
                        if (TableColumn.doesTableColumnNameAlreadyExists(tableColumns, textFieldPrimaryColumnName.getText())) {
                            CustomAlerts.warning("Column name already exists", "Choose a different column name");
                        }
                        newColumn = new TableColumn(textFieldPrimaryColumnName.getText(),
                                new Pair<>(choiceBoxSqlType.getSelectionModel().getSelectedItem(),
                                        textFieldRustType.getText()), checkBoxNullAllowed.isSelected(),
                                checkBoxPrimary.isSelected());
                    }
                    case TABLE_EDIT -> {
                    }
                    case COLUMN_EDIT -> {
                        String sqlType = tableColumn.getSqlRustType().key
                                .equals(choiceBoxSqlType.getSelectionModel().getSelectedItem()) ?
                                tableColumn.getSqlRustType().key : choiceBoxSqlType.getSelectionModel().getSelectedItem();
                        newColumn = new TableColumn(tableColumn.getName(), new Pair<>(sqlType, SqlRustType.getRustType(sqlType)),
                                checkBoxNullAllowed.isSelected(), checkBoxPrimary.isSelected());
                    }
                }
                if (TableDialogControllerMethods.onSave(newTable, newColumn, editType)) {
                    Stage stage = (Stage) buttonSave.getScene().getWindow();
                    stage.close();
                }
            }
        }));
        buttonCancel.setOnAction(e -> {
            Stage stage = (Stage) buttonCancel.getScene().getWindow();
            stage.close();
        });
        Platform.runLater(() -> {
            if (editType == EditType.TABLE_EDIT || editType == EditType.COLUMN_EDIT) {
                if (editType == EditType.TABLE_EDIT) {
                    textFieldTableName.setText(table.getName());
                    textFieldPrimaryColumnName.setText(table.getPrimaryColumn().getName());
                    checkBoxPrimary.setSelected(table.getPrimaryColumn().isPrimary());
                    checkBoxNullAllowed.setSelected(table.getPrimaryColumn().isNullAllowed());
                } else {
                    textFieldPrimaryColumnName.setText(tableColumn.getName());
                    textFieldPrimaryColumnName.setEditable(false);
                    textFieldTableName.setVisible(false);
                    labelTableName.setVisible(false);
                    labelColumnName.setText("Column name");
                    checkBoxNullAllowed.setSelected(tableColumn.isNullAllowed());
                    checkBoxPrimary.setSelected(tableColumn.isPrimary());
                    choiceBoxSqlType.getSelectionModel().select(tableColumn.getSqlRustType().key);
                }
            } else if (editType == EditType.COLUMN) {
                textFieldTableName.setVisible(false);
                labelColumnName.setText("Column name");
            }
        });
    }

    public void setTables(List<Table> tables) {
        this.tables = tables;
    }

    public void setTableColumns(Queue<TableColumn> tableColumns) {
        this.tableColumns = tableColumns;
    }

    public void setTable(Table table) {
        this.table = table;
    }

    public void setTableColumn(TableColumn tableColumn) {
        this.tableColumn = tableColumn;
    }

    public Table getNewTable() {
        return newTable;
    }

    public TableColumn getNewColumn() {
        return newColumn;
    }

    public void setEditType(EditType editType) {
        this.editType = editType;
    }

    public enum EditType {
        TABLE, TABLE_EDIT, COLUMN, COLUMN_EDIT
    }
}
