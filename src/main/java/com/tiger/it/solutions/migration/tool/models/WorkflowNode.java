package com.tiger.it.solutions.migration.tool.models;

public class WorkflowNode {

    public final String id;
    private WorkflowNodeType type;

    public WorkflowNode(String id, WorkflowNodeType type) {
        this.id = id;
        this.type = type;
    }

    public enum WorkflowNodeType {
        START, END, NODE, SUB_WORKFLOW
    }
}
