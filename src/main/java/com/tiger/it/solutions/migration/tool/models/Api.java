package com.tiger.it.solutions.migration.tool.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Api implements ModuleParser<Api> {

    public final String id;
    private String name;

    private String version;

    private String versionRegex;

    public Api() {
        this.id = "";
    }

    public Api(String id, String name, String version, String versionRegex) {
        this.id = id;
        this.name = name;
        this.version = version;
        this.versionRegex = versionRegex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersionRegex() {
        return versionRegex;
    }

    public void setVersionRegex(String versionRegex) {
        this.versionRegex = versionRegex;
    }

    @Override
    public List<Api> parse(String json) {
        List<Api> apis = new ArrayList<>();

        JSONObject root = new JSONObject(json);
        JSONArray jsonApis = root.getJSONArray("output");

        for (int i = 0; i < jsonApis.length(); ++i) {
            JSONObject temp = jsonApis.getJSONObject(i);
            apis.add(new Api(temp.getString("uuid"), temp.getString("name"), temp.getString("version"),
                    temp.getString("version_regex")));
        }

        return apis;
    }

    @Override
    public String toJson() {
        return null;
    }

    @Override
    public String listToJson(List<Api> objects, boolean feedback) {
        return null;
    }
}
