package com.tiger.it.solutions.migration.tool.models;

import java.util.Map;
import java.util.Optional;

public class SqlRustType {

    private SqlRustType() {
    }

    public static Map<String, String> sqlRustTypeMapping() {
        return Map.of("DOUBLE", "f64", "BLOB", "String", "VARCHAR", "StringV",
                "INT", "i64", "TEXT", "String8", "TEXT16", "String16",
                "CHAR", "char", "TINYINT", "bool", "DATE", "chrono::Date",
                "DATETIME", "chrono::DateT");
    }

    public static String getRustType(String sqlType) {
        Optional<Map.Entry<String, String>> op = SqlRustType.sqlRustTypeMapping().entrySet().stream()
                .filter(e -> e.getKey().equals(sqlType)).findFirst();
        return op.get().getValue();
    }
}
