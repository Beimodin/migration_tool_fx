package com.tiger.it.solutions.migration.tool.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ModuleFunctionClass implements ModuleParser<ModuleFunctionClass> {

    private String functionName;
    private String functionSignature;
    private String functionDocumentation;
    private ModuleTypeFunctionClass moduleTypeFunctionClass;

    private String moduleFunctionClassInformationInHtml;

    public ModuleFunctionClass(String functionName, String functionSignature, String functionDocumentation,
                               ModuleTypeFunctionClass moduleTypeFunctionClass, String moduleFunctionClassInformationInHtml) {
        this.functionName = functionName;
        this.functionSignature = functionSignature;
        this.functionDocumentation = functionDocumentation;
        this.moduleTypeFunctionClass = moduleTypeFunctionClass;
        this.moduleFunctionClassInformationInHtml = moduleFunctionClassInformationInHtml;
    }

    public String getFunctionName() {
        return functionName;
    }

    public String getFunctionSignature() {
        return functionSignature;
    }

    public String getFunctionDocumentation() {
        return functionDocumentation;
    }

    public String getModuleFunctionClassInformationInHtml() {
        return moduleFunctionClassInformationInHtml;
    }

    public ModuleTypeFunctionClass getModuleTypeFunctionClass() {
        return moduleTypeFunctionClass;
    }

    @Override
    public List<ModuleFunctionClass> parse(String json) {
        return new ArrayList<>();
    }

    @Override
    public String toJson() {
        return null;
    }

    @Override
    public String listToJson(List<ModuleFunctionClass> objects, boolean feedback) {
        return null;
    }

    public enum ModuleTypeFunctionClass {
        CLASS, FUNCTION
    }
}
