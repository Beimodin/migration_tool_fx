package com.tiger.it.solutions.migration.tool.utils;

import java.util.Map;

public interface HtmlGenerator {

    String generateHtml();

    class HtmlMethods {

        public final Map<String, String> htmls = Map.ofEntries(
                Map.entry("DOCTYPE", "<!DOCTYPE>"),
                Map.entry("SHTML", "<html>"),
                Map.entry("EHTML", "</html>"),
                Map.entry("SBODY", "<body>"),
                Map.entry("EBODY", "</body>")
        );
        private HtmlMethods() {}

        public String getHtmlMethod(String name) {
            return this.htmls.get(name);
        }
    }
}
