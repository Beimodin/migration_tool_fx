package com.tiger.it.solutions.migration.tool.views.main.sqlworkbench.tablerelationship;

import com.tiger.it.solutions.migration.tool.models.Table;
import com.tiger.it.solutions.migration.tool.models.TableRelationship;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.util.*;

public class TableRelationshipControllerMethods {

    private TableRelationshipControllerMethods() {
    }

    public static void setTableViewPolicy(TableView<com.tiger.it.solutions.migration.tool.models.TableColumn> tableView) {
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tableView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
    }

    public static void setRelationTypes(ChoiceBox<TableRelationship.RelationshipType> choiceBox) {
        choiceBox.setItems(FXCollections.observableList(Arrays.stream(TableRelationship.RelationshipType.values()).toList()));
    }

    public static void setTables(ChoiceBox<String> choiceBox, List<Table> tables) {
        List<String> temp = new ArrayList<>();
        tables.forEach(e -> temp.add(e.getName()));
        Collections.sort(temp);
        choiceBox.setItems(FXCollections.observableList(temp));
    }

    public static void setTableColumnParentProperties(TableColumn<com.tiger.it.solutions.migration.tool.models.TableColumn, String> name,
                                                      TableColumn<com.tiger.it.solutions.migration.tool.models.TableColumn, String> sqlType,
                                                      TableColumn<com.tiger.it.solutions.migration.tool.models.TableColumn, String> rustType) {
        name.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
        sqlType.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getSqlRustType().key));
        rustType.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getSqlRustType().value));
    }

    public static void setTableColumnChildProperties(TableColumn<com.tiger.it.solutions.migration.tool.models.TableColumn, String> name,
                                                     TableColumn<com.tiger.it.solutions.migration.tool.models.TableColumn, String> sqlType,
                                                     TableColumn<com.tiger.it.solutions.migration.tool.models.TableColumn, String> rustType) {
        name.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
        sqlType.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getSqlRustType().key));
        rustType.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getSqlRustType().value));
    }

    public static boolean onSave(TableRelationship tableRelationship) {
        if (tableRelationship.getChildTable() == null) return false;
        if (tableRelationship.getParentTable() == null) return false;
        if (tableRelationship.getChildTableColumn() == null) return false;
        if (tableRelationship.getParentTableColumn() != null) return false;
        return true;
    }

    public static void onTableColumnChange(ChoiceBox<String> choiceBox, TableView<com.tiger.it.solutions.migration.tool.models.TableColumn> tableView, List<Table> tables) {
        choiceBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            tableView.getItems().clear();
            Optional<Table> table = tables.stream()
                    .filter(e -> e.getName().equals(newValue)).findFirst();
            table.ifPresent(value -> tableView.getItems().addAll(FXCollections.observableList(value.getColumns().stream().toList())));
        });
    }
}
