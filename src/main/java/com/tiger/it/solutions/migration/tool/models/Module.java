package com.tiger.it.solutions.migration.tool.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Module implements ModuleParser<Module> {

    private static final Logger LOGGER = Logger.getLogger(Module.class.getName());

    public final String id;
    private String name;
    private ModuleType type;

    private String description;
    private boolean enable;
    private List<String> requiredBy;

    private String path;

    private String content;

    private List<ModuleFunctionClass> moduleFunctionClasses;

    public Module() {
        this.id = "";
    }

    public Module(String id, String name, ModuleType type, String description, boolean enable, List<String> requiredBy,
                  String path, String content, List<ModuleFunctionClass> moduleFunctionClasses) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.description = description;
        this.enable = enable;
        this.requiredBy = requiredBy;
        this.path = path;
        this.content = content;
        this.moduleFunctionClasses = moduleFunctionClasses;
    }

    public Module(String id, String name, ModuleType type, String description, boolean enable, List<String> requiredBy,
                  String path, String content) {
        this(id, name, type, description, enable, requiredBy, path, content, new ArrayList<>());
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ModuleType getType() {
        return type;
    }

    public void setType(ModuleType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public List<String> getRequiredBy() {
        return requiredBy;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<ModuleFunctionClass> getModuleFunctionClasses() {
        return moduleFunctionClasses;
    }

    public void setModuleFunctionClasses(List<ModuleFunctionClass> moduleFunctionClasses) {
        this.moduleFunctionClasses = moduleFunctionClasses;
    }

    public void parseModuleFunctionClasses(String json) {
        List<ModuleFunctionClass> functionClasses = new ArrayList<>();
        JSONObject root = new JSONObject(json);
        JSONObject output = root.getJSONObject("output");
        JSONArray modules = output.getJSONArray("modules");

        for (int i = 0; i < modules.length(); ++i) {
            JSONObject temp = modules.getJSONObject(i);

            if (temp.getString("name").equals(this.name)) {
                JSONArray functions = temp.getJSONArray("functions");
                JSONArray classes = temp.getJSONArray("classes");

                for (int j = 0; j < functions.length(); ++i) {
                    JSONObject func = functions.getJSONObject(j);
                    ModuleFunctionClass mfc = new ModuleFunctionClass(func.getString("name"), func.getString("signature"),
                            null, ModuleFunctionClass.ModuleTypeFunctionClass.FUNCTION, "");
                    functionClasses.add(mfc);
                }

                for (int j = 0; j < classes.length(); ++i) {
                    JSONObject clazz = classes.getJSONObject(j);
                    ModuleFunctionClass mfc = new ModuleFunctionClass(clazz.getString("name"), clazz.getString("signature"),
                            null, ModuleFunctionClass.ModuleTypeFunctionClass.CLASS, "");
                    functionClasses.add(mfc);
                }
            }
        }

        if (!functionClasses.isEmpty())
            this.moduleFunctionClasses.clear();

        this.moduleFunctionClasses.addAll(functionClasses);
    }

    public String getRequiredByWithSeparator() {
        StringBuilder builder = new StringBuilder();
        requiredBy.forEach(s -> builder.append(String.format("%s,", s)));
        builder.deleteCharAt(builder.length() - 1);
        return builder.toString();
    }

    public static String getContentByLength(File file) {
        String content;
        try {
            content = file.length() > 3000 ? Files.readString(file.toPath(), StandardCharsets.UTF_8) : "";
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
            content = "";
        }
        return content;
    }

    public String limitDescription() {
        return description.length() > 25 ? description.substring(0, 25) : description;
    }

    public static ModuleType getTypeFromFileExtension(String fileName, String extension) {
        switch (extension) {
            case "rs" -> {
                if (fileName.startsWith("M_")) {
                    return ModuleType.MODULE;
                } else if (fileName.startsWith("L_")) {
                    return ModuleType.LIBRARY;
                }
            }
            case "sh", "ps1", "ps", "cmd" -> {
                return ModuleType.SCRIPT;
            }
            case "json" -> {
                return ModuleType.TOOL;
            }
        }
        return ModuleType.UNKNOWN;
    }

    public static String getPathByExtension(ModuleType type) {
        return switch (type) {
            case TOOL -> "%PROJECT_NAME_STRUCTURE%/tools/";
            case MODULE -> "%PROJECT_NAME_STRUCTURE%/modules/";
            case SCRIPT -> "%PROJECT_NAME_STRUCTURE%/scripts/";
            case LIBRARY -> "%PROJECT_NAME_STRUCTURE%/libraries/";
            default -> null;
        };
    }

    @Override
    public List<Module> parse(String json) {
        List<Module> modules = new ArrayList<>();

        try {
            JSONObject root = new JSONObject(json);
            JSONArray jsonModules = root.getJSONArray("output");

            for (int i = 0; i < jsonModules.length(); ++i) {
                JSONObject temp = jsonModules.getJSONObject(i);
                List<String> requiredBy = new ArrayList<>();
                JSONArray rb = temp.getJSONArray("required_by");

                for (int j = 0; j < rb.length(); ++j) {
                    requiredBy.add(rb.getString(i));
                }

                Module module = new Module(temp.getString("uuid"), temp.getString("name"),
                        ModuleType.valueOf(temp.getString("type")), temp.getString("description"),
                        temp.getBoolean("enabled"), requiredBy, temp.getString("path"),
                        temp.getString("content"));

                modules.add(module);
            }
        } catch (JSONException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
            return new ArrayList<>();
        }

        return modules;
    }

    @Override
    public String toJson() {
        JSONObject object = new JSONObject();
        object.put("id", this.id);
        object.put("name", this.name);
        object.put("type", this.type.toString());
        object.put("description", this.description);
        object.put("enabled", this.enable);

        JSONArray temp = new JSONArray();

        this.requiredBy.forEach(temp::put);
        object.put("required_by", temp);

        object.put("path", this.path);
        object.put("content", this.content);
        return object.toString();
    }

    @Override
    public String listToJson(List<Module> objects, boolean feedback) {
        return null;
    }

    public enum ModuleType {
        LIBRARY, MODULE, SCRIPT, TOOL, UNKNOWN
    }

    private String generateModuleInformationAsHtml(ModuleFunctionClass mfc) {

        return null;
    }
}
