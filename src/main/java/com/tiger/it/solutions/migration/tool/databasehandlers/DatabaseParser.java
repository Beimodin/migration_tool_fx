package com.tiger.it.solutions.migration.tool.databasehandlers;

import com.tiger.it.solutions.migration.tool.models.*;
import com.tiger.it.solutions.migration.tool.models.Module;
import com.tiger.it.solutions.migration.tool.utils.CustomAlerts;
import com.tiger.it.solutions.migration.tool.utils.HttpHandler;
import com.tiger.it.solutions.migration.tool.utils.JsonHandler;
import javafx.application.Platform;
import javafx.scene.control.Alert;

import java.io.IOException;
import java.net.InetAddress;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DatabaseParser implements Database, Runnable {

    private static final Logger LOGGER = Logger.getLogger(DatabaseParser.class.getName());

    private final HttpHandler httpHandler;
    private ScheduledThreadPoolExecutor databaseChecker = null;

    public DatabaseParser(Config.ServerConfig serverConfig) {
        httpHandler = new HttpHandler(serverConfig.getUrl());
    }

    private String execute(HttpHandler.HttpType httpType, String url, String body) {
        try {
            if (body.isEmpty()) {
                return httpHandler.executeRequest(httpType, url);
            }
            return httpHandler.executeRequest(httpType, url, body);
        } catch (IOException | ExecutionException | InterruptedException ex) {
            LOGGER.log(Level.INFO, ex.getMessage(), ex);
        }
        return "{\"output\": []}";
    }

    @Override
    public void startCheckSystem() {
        if (databaseChecker == null) {
            databaseChecker = new ScheduledThreadPoolExecutor(1);
            databaseChecker.scheduleAtFixedRate(this, 0, 5, TimeUnit.MINUTES);
        }
    }

    @Override
    public List<Runnable> stopCheckSystem() {
        if (databaseChecker != null) {
            databaseChecker.shutdown();
            try {
                databaseChecker.awaitTermination(5, TimeUnit.SECONDS);
            } catch (InterruptedException ex) {
                LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
            }
            return databaseChecker.shutdownNow();
        }
        return new ArrayList<>();
    }

    @Override
    public List<Table> getTables() {
        String temp = execute(HttpHandler.HttpType.GET, "tables", "");
        return new Table().parse(temp);
    }

    @Override
    public Queue<TableColumn> getTableColumns(String tableName) {
        String temp = execute(HttpHandler.HttpType.GET, String.format("table_columns?name=%s", tableName), "");
        return new ArrayDeque<>(new TableColumn().parse(temp));
    }

    @Override
    public String removeTable(Table table) {
        return execute(HttpHandler.HttpType.DELETE, String.format("table?name=%s", table.getName()), "");
    }

    @Override
    public String addTable(Table table) {
        return execute(HttpHandler.HttpType.POST, "table", JsonHandler.createRequestBody(
                new Pair("table", table.toJson())));
    }

    @Override
    public String addTableColumn(Table table, TableColumn column) {
        return execute(HttpHandler.HttpType.PUT, "table_column", JsonHandler.createRequestBody(
                new Pair("table", table.getName()), new Pair("table_column", column.toJson())));
    }

    @Override
    public String removeTableColumn(TableColumn column) {
        return execute(HttpHandler.HttpType.DELETE, "table_column", JsonHandler.createRequestBody(
                new Pair("table_column", column.getName())));
    }

    @Override
    public String changePrimaryColumn(Table table, TableColumn newPrimaryColumn, TableColumn oldPrimaryColumn) {
        return null;
    }

    @Override
    public String changeIsNullAllowed(TableColumn column, boolean isNullAllowed) {
        return null;
    }

    @Override
    public String changeIsPrimaryColumn(TableColumn column, boolean isPrimary) {
        return null;
    }

    @Override
    public String updateTableColumn(TableColumn oldColumn, TableColumn newColumn) {
        return null;
    }

    @Override
    public List<TableRelationship> getTableRelationships(List<Table> tables) {
        String temp = execute(HttpHandler.HttpType.GET, "table_relation_ship", "");
        return new TableRelationship().parse(temp);
    }

    @Override
    public String addTableRelationship(TableRelationship tableRelationship) {
        return execute(HttpHandler.HttpType.POST, "table_relation_ship", JsonHandler.createRequestBody(
                new Pair("table_relation_ship", tableRelationship.toJson())));
    }

    @Override
    public String removeTableRelationship(TableRelationship tableRelationship) {
        return null;
    }

    @Override
    public String updateTableRelationship(TableRelationship oldTableRelationship, TableRelationship newTableRelationship) {
        return null;
    }

    @Override
    public List<Module> getModules() {
        String temp = execute(HttpHandler.HttpType.GET, "module", "");
        return new Module().parse(temp);
    }

    @Override
    public String addModule(Module module) {
        return execute(HttpHandler.HttpType.POST, "module", JsonHandler.createRequestBody(
                new Pair("module", module.toJson())));
    }

    @Override
    public String updateModule(Module oldModule, Module newModule) {
        return null;
    }

    @Override
    public String removeModule(Module module) {
        return null;
    }

    @Override
    public List<Workflow> getWorkflows() {
        String temp = execute(HttpHandler.HttpType.GET, "workflow", "");
        return new Workflow().parse(temp);
    }

    @Override
    public String addWorkflow(Workflow workflow) {
        return null;
    }

    @Override
    public String updateWorkflow(Workflow oldWorkflow, Workflow newWorkflow) {
        return null;
    }

    @Override
    public String removeWorkflow(Workflow workflow) {
        return null;
    }

    @Override
    public List<Workflow.WorkflowNode> getWorkflowNodes() {
        return null;
    }

    @Override
    public String addWorkflowNode(Workflow.WorkflowNode workflowNode) {
        return null;
    }

    @Override
    public String updateWorkflow(Workflow.WorkflowNode oldWorkflowNode, Workflow.WorkflowNode newWorkflowNode) {
        return null;
    }

    @Override
    public List<Workflow.WorkflowHistory> getWorkflowHistory() {
        return null;
    }

    @Override
    public Workflow.WorkflowHistory getLatestWorkflow() {
        return null;
    }

    @Override
    public Workflow.WorkflowHistory getWorkflow(Workflow.WorkflowHistory workflowHistory) {
        return null;
    }

    @Override
    public String addWorkflowHistory(Workflow.WorkflowHistory workflowHistory) {
        return null;
    }

    @Override
    public String updateWorkflowHistory(Workflow.WorkflowHistory oldWorkflowHistory, Workflow.WorkflowHistory newWorkflowHistory) {
        return null;
    }

    @Override
    public String removeWorkflowHistory(Workflow.WorkflowHistory workflowHistory) {
        return null;
    }

    @Override
    public List<LogType> getLogTypes() {
        return null;
    }

    @Override
    public List<Workflow.EndSequence> getWorkflowEndSequence(Workflow workflow) {
        return null;
    }

    @Override
    public String addEndSequence(Workflow.EndSequence endSequence, Workflow workflow) {
        return null;
    }

    @Override
    public String updateEndSequence(Workflow.EndSequence oldEndSequence, Workflow.EndSequence newEndSequence, Workflow workflow) {
        return null;
    }

    @Override
    public List<Workflow.Waiting> getWaitings() {
        return null;
    }

    @Override
    public String addWaiting(Workflow.Waiting waiting, Workflow workflow) {
        return null;
    }

    @Override
    public String updateWaiting(Workflow.Waiting oldWaiting, Workflow.Waiting newWaiting) {
        return null;
    }

    @Override
    public String removeWaiting(Workflow.Waiting waiting) {
        return null;
    }

    @Override
    public List<Variable> getVariables() {
        return null;
    }

    @Override
    public List<Variable> getVariablesByWorkflow(Workflow workflow) {
        return null;
    }

    @Override
    public String updateVariable(Variable oldVariable, Variable newVariable) {
        return null;
    }

    @Override
    public String removeVariable(Variable variable) {
        return null;
    }

    @Override
    public List<Datatype> getDatatypes() {
        return null;
    }

    @Override
    public String updateDatatype(Datatype oldDatatype, Datatype newDatatype) {
        return null;
    }

    @Override
    public String removeDatatype(Datatype datatype) {
        return null;
    }

    @Override
    public List<Action> getActions() {
        String temp = execute(HttpHandler.HttpType.GET, "module", "");
        return new Action().parse(temp);
    }

    @Override
    public List<Api> getApis() {
        String temp = execute(HttpHandler.HttpType.POST, "api", "");
        return new Api().parse(temp);
    }

    @Override
    public String updateApi(Api oldApi, Api newApi) {
        return null;
    }

    @Override
    public String removeApi(Api api) {
        return null;
    }

    @Override
    public List<ApiType> getApiTypes() {
        return null;
    }

    @Override
    public List<ApiType> getApiTypesByApi(String apiId) {
        return null;
    }

    @Override
    public String updateApiType(ApiType oldApiType, ApiType newApiType) {
        return null;
    }

    @Override
    public String removeApiType(ApiType apiType) {
        return null;
    }

    @Override
    public String onClose() {
        return null;
    }

    @Override
    public void run() {
        Platform.runLater(() -> {
            try {
                httpHandler.executeRequest(HttpHandler.HttpType.GET, "ping-me");
                System.out.println("s");
            } catch (IOException | ExecutionException | InterruptedException ex) {
                CustomAlerts.alert(Alert.AlertType.ERROR, "Server unreachable", "Server is unreachable.");
                LOGGER.log(Level.SEVERE, String.format("Server unreachable %s", ex.getMessage()), ex);
            }
        });
    }
}
