package com.tiger.it.solutions.migration.tool.views.main.menuitems.apitools.workflow.newworkflow;

import com.tiger.it.solutions.migration.tool.databasehandlers.Database;
import com.tiger.it.solutions.migration.tool.models.Config;
import com.tiger.it.solutions.migration.tool.models.WorkflowFile;
import com.tiger.it.solutions.migration.tool.models.WorkflowSetting;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;


public class NewApiWorkflowController implements Initializable {

    @FXML
    private GridPane gridPaneApiWorkflowInformation;

    @FXML
    private GridPane gridPaneApiWorkflowStructures;

    @FXML
    private GridPane gridPaneApiTypeSelectors;

    @FXML
    private TextField textFieldApiWorkflowId;

    @FXML
    private TextField textFieldApiWorkflowName;

    @FXML
    private TextField textFieldApiWorkflowVersion;

    @FXML
    private TextField textFieldApiWorkflowVersionRegex;

    @FXML
    private TextArea textAreaApiWorkflowJsonPreview;

    @FXML
    private Label labelApiWorkflowId;

    @FXML
    private Label labelApiWorkflowName;

    @FXML
    private Label labelApiWorkflowVersion;

    @FXML
    private Label labelApiWorkflowVersionRegex;

    @FXML
    private Label labelApiWorkflowStructureCurrentAddedFileCount;

    @FXML
    private Button buttonApiWorkflowStructureAutoIgnoreFields;

    @FXML
    private Button buttonApiWorkflowStructurePreprocessingErrorMoreInformation;

    @FXML
    private Button buttonApiWorkflowStructurePreviewInternalStructureLayout;

    @FXML
    private Button buttonApiWorkflowStructureInternalStructureLayoutHelp;

    @FXML
    private Button buttonApiWorkflowStructureAddStructure;

    @FXML
    private Button buttonSaveWorkflow;

    @FXML
    private Button buttonCancelSaveWorkflow;

    @FXML
    private Button buttonApiWorkflowStructurePreprocessStructure;

    @FXML
    private CheckBox checkBoxApiWorkflowStructureCreateUnitTests;

    @FXML
    private Text textApiWorkflowStructurePreprocessingErrorMessage;

    @FXML
    private TableView<WorkflowFile> tableViewWorkflowStructureFiles;
    //private TableView<WorkflowStructure> tableViewStructureFields;

    @FXML
    private TableView<WorkflowSetting> tableViewWorkflowSettings;

    @FXML
    private TableColumn<WorkflowFile, String> tableColumnWorkflowStructureFilesFileName;

    @FXML
    private TableColumn<WorkflowFile, String> tableColumnWorkflowStructureFilesActivateFields;

    @FXML
    private TableColumn<WorkflowFile, String> tableColumnWorkflowStructureFilesIgnoredFields;

    @FXML
    private TableColumn<WorkflowFile, String> tableColumnWorkflowStructureFilesFileType;

    @FXML
    private TableColumn<WorkflowFile, String> tableColumnWorkflowStructureFilesUnitTests;

    /*@FXML
    private TableColumn<WorkflowStructure, String> tableColumnStructureFieldsFieldName;

    @FXML
    private TableColumn<WorkflowStructure, String> tableColumnStructureFieldsValueType;

    @FXML
    private TableColumn<WorkflowStructure, String> tableColumnStructureFieldsCurrentValue;

    @FXML
    private TableColumn<WorkflowStructure, String> tableColumnStructureFieldsNewValue;

    @FXML
    private TableColumn<WorkflowStructure, String> tableColumnStructureFieldsIgnoreField;*/

    @FXML
    private TableColumn<WorkflowSetting, String> tableColumnWorkflowSettingsName;

    @FXML
    private TableColumn<WorkflowSetting, String> tableColumnWorkflowSettingsValue;

    @FXML
    private TableColumn<WorkflowSetting, String> tableColumnWorkflowSettingsDefaultValue;

    @FXML
    private TableColumn<WorkflowSetting, String> tableColumnWorkflowSettingsGlobalInherited;

    @FXML
    private TableColumn<WorkflowSetting, String> tableColumnWorkflowSettingsOverwriteGlobal;

    @FXML
    private HBox hBoxApiText;

    @FXML
    private HBox hBoxTypeText;

    @FXML
    private HBox hBoxBottomButtons;

    @FXML
    private VBox vBoxSettings;

    @FXML
    private ChoiceBox<String> choiceBoxApis;

    @FXML
    private ChoiceBox<String> choiceBoxApiTypes;

    @FXML
    private Database database;

    @FXML
    private Config config;

    private String fileStructure;

    @Override
    public void initialize(URL location, ResourceBundle resources) {


    }

    public void setDatabase(Database database) {
        this.database = database;
    }

    public void setConfig(Config config) {
        this.config = config;
    }
}
