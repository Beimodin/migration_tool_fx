package com.tiger.it.solutions.migration.tool.models;

public class WorkflowFile {

    public final String id;

    private String fileName;

    private int activateFields;

    private int ignoredFields;

    private String fileType;

    private boolean unitTestsEnabled;

    public WorkflowFile(String id, String fileName, int activateFields, int ignoredFields, String fileType,
                        boolean unitTestsEnabled) {
        this.id = id;
        this.fileName = fileName;
        this.activateFields = activateFields;
        this.ignoredFields = ignoredFields;
        this.fileType = fileType;
        this.unitTestsEnabled = unitTestsEnabled;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getActivateFields() {
        return activateFields;
    }

    public void setActivateFields(int activateFields) {
        this.activateFields = activateFields;
    }

    public int getIgnoredFields() {
        return ignoredFields;
    }

    public void setIgnoredFields(int ignoredFields) {
        this.ignoredFields = ignoredFields;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public boolean isUnitTestsEnabled() {
        return unitTestsEnabled;
    }

    public void setUnitTestsEnabled(boolean unitTestsEnabled) {
        this.unitTestsEnabled = unitTestsEnabled;
    }
}
