package com.tiger.it.solutions.migration.tool.views;

import com.tiger.it.solutions.migration.tool.databasehandlers.Database;
import com.tiger.it.solutions.migration.tool.databasehandlers.DatabaseParser;
import com.tiger.it.solutions.migration.tool.models.Config;
import com.tiger.it.solutions.migration.tool.utils.ConfigLoader;
import com.tiger.it.solutions.migration.tool.utils.CustomAlerts;
import javafx.application.Application;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

import java.io.IOException;
import java.time.Duration;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GUI extends Application {

    private static final Logger LOGGER = Logger.getLogger(GUI.class.getName());

    private static String config = null;
    private static Timer databaseChecker = new Timer();

    @Override
    public void start(Stage primaryStage) throws Exception {
        Config config;

        try {
            config = ConfigLoader.loadConfig(GUI.config);
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
            return;
        }

        Database database = new DatabaseParser(config.getServerConfig());

        ViewLoader.loadMainView(primaryStage, database);
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            CustomAlerts.alert(Alert.AlertType.ERROR, "No config found", "Config path not set");
            LOGGER.log(Level.SEVERE, "Config path not set");
            return;
        }
        config = args[0];
        launch(args);
    }
}
