package com.tiger.it.solutions.migration.tool.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Workflow implements ModuleParser<Workflow> {

    private static final Logger LOGGER = Logger.getLogger(Workflow.class.getName());

    public final String name;

    public final Date created;

    private Date lastChanged;

    private boolean preLoadCheck;

    private boolean createLog;


    public Workflow() {
        this("", new Date(), new Date(), false, false);
    }

    public Workflow(String name, Date created, Date lastChanged, boolean preLoadCheck, boolean createLog) {
        this.name = name;
        this.created = created;
        this.lastChanged = lastChanged;
        this.preLoadCheck = preLoadCheck;
        this.createLog = createLog;
    }

    public static class WorkflowHistory {
    }

    public static class WorkflowNode {
    }

    public static class EndSequence {
    }

    public static class Waiting {
    }

    @Override
    public List<Workflow> parse(String json) {
        List<Workflow> workflows = new ArrayList<>();

        try {
            JSONObject root = new JSONObject(json);
            JSONArray jsonWorkflows = root.getJSONArray("output");

            DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

            for (int i = 0; i < jsonWorkflows.length(); ++i) {
                JSONObject temp = jsonWorkflows.getJSONObject(i);
                Date created = format.parse(temp.getString("created"));
                Date lastChanged = format.parse(temp.getString("last_changed"));
                Workflow workflow = new Workflow(temp.getString("name"), created, lastChanged,
                        temp.getBoolean("pre_load_check"), temp.getBoolean("create_log"));
                workflows.add(workflow);
            }
        } catch (JSONException | ParseException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
            return new ArrayList<>();
        }
        return workflows;
    }

    @Override
    public String toJson() {
        JSONObject object = new JSONObject();
        object.put("name", this.name);
        object.put("created", this.created);
        object.put("last_changed", this.lastChanged);
        object.put("pre_load_check", this.preLoadCheck);
        object.put("create_log", this.createLog);
        return object.toString();
    }

    @Override
    public String listToJson(List<Workflow> objects, boolean feedback) {
        JSONObject root = new JSONObject();
        JSONArray array = new JSONArray();
        objects.forEach(workflow -> array.put(workflow.toJson()));
        root.put("workflows", array);
        return root.toString();
    }
}
