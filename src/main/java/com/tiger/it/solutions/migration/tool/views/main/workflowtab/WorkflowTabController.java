package com.tiger.it.solutions.migration.tool.views.main.workflowtab;

import com.tiger.it.solutions.migration.tool.databasehandlers.Database;
import com.tiger.it.solutions.migration.tool.models.Workflow;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.ResourceBundle;

public class WorkflowTabController implements Initializable {

    @FXML
    private TabPane tabPaneWorkflows;

    @FXML
    private Tab tabParentWorkflow;

    @FXML
    private VBox vboxWorkflowNodes;

    @FXML
    private ListView<String> listViewWorkflowNodes;

    @FXML
    private ListView<Workflow> listViewWorkflows;

    @FXML
    private Database database;

    private ObservableList<Workflow> workflows;
    private ObservableList<String> actions;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        listViewWorkflowNodes.setDisable(true);
        Platform.runLater(() -> {
            workflows = WorkflowTabControllerMethods.loadAvailableWorkflows(listViewWorkflows, database);
            actions = WorkflowTabControllerMethods.loadNodeActions(listViewWorkflowNodes, database);
            WorkflowTabControllerMethods.setWorkflowListViewProperty(listViewWorkflows);
            //WorkflowTabControllerMethods.setActionListViewProperty(listViewWorkflowNodes);
            WorkflowTabControllerMethods.setListViewWorkflowsListener(listViewWorkflows, listViewWorkflowNodes);
        });
    }

    public void setDatabase(Database database) {
        this.database = database;
    }
}
