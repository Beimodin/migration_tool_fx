package com.tiger.it.solutions.migration.tool.views.main.sqlworkbench;

import com.tiger.it.solutions.migration.tool.databasehandlers.Database;
import com.tiger.it.solutions.migration.tool.models.Table;
import com.tiger.it.solutions.migration.tool.models.TableRelationship;
import com.tiger.it.solutions.migration.tool.utils.CustomAlerts;
import com.tiger.it.solutions.migration.tool.views.main.sqlworkbench.tabledialog.TableDialogController;
import com.tiger.it.solutions.migration.tool.views.main.sqlworkbench.tablerelationship.TableRelationshipController;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.tiger.it.solutions.migration.tool.views.ViewLoader.loadStageAndWait;

public class SqlWorkbenchControllerMethods {

    private static final Logger LOGGER = Logger.getLogger(SqlWorkbenchControllerMethods.class.getName());

    private SqlWorkbenchControllerMethods() {
    }

    public static void setTableViewResizePolicy(TableView tableView) {
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tableView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
    }

    public static void setTableColumnsProperty(TableColumn<Table, String> id, TableColumn<Table, String> name,
                                               TableColumn<Table, String> primaryColumn,
                                               TableColumn<Table, String> columnSize,
                                               TableColumn<Table, String> edit,
                                               TableColumn<Table, String> delete, Database database) {
        //id.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.));
        name.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
        primaryColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue()
                .getPrimaryColumn().getName()));
        columnSize.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue()
                .getColumns().size())));

        Callback<TableColumn<Table, String>, TableCell<Table, String>> cellFactoryDelete = new Callback<>() {
            @Override
            public TableCell<Table, String> call(TableColumn<Table, String> param) {
                return new TableCell<>() {
                    final Button button = new Button();

                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!empty) {
                            button.setText("Remove");
                            button.setOnAction(e -> {
                                if (CustomAlerts.confirmation("Remove Table", "This action cannot be reversed!",
                                        Alert.AlertType.WARNING)) {
                                    int index = indexProperty().getValue() < 0 ? 0 : indexProperty().getValue();
                                    Table correctItem = getTableView().getItems().get(index);
                                    database.removeTable(correctItem);
                                    getTableView().getItems().remove(correctItem);
                                    getTableView().refresh();
                                    LOGGER.info("Removed table " + correctItem.getName());
                                }
                            });
                            setGraphic(button);
                        }
                        setText(null);
                    }
                };
            }
        };
        delete.setCellFactory(cellFactoryDelete);

        Callback<TableColumn<Table, String>, TableCell<Table, String>> cellFactoryEdit = new Callback<>() {
            @Override
            public TableCell<Table, String> call(TableColumn<Table, String> param) {
                return new TableCell<>() {
                    final Button button = new Button();

                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);

                        if (!empty) {
                            button.setText("Edit");
                            button.setOnAction(e -> {
                                try {
                                    FXMLLoader loader = new FXMLLoader(SqlWorkbenchControllerMethods.class.getResource(""));
                                    AnchorPane pane = loader.load();
                                    TableDialogController controller = loader.getController();
                                } catch (IOException ex) {
                                    LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
                                }
                            });
                            setGraphic(button);
                        }
                        setText(null);
                    }
                };
            }
        };
        edit.setCellFactory(cellFactoryEdit);
    }

    public static void setTableRelationColumnProperties(TableColumn<TableRelationship, String> parentTable,
                                                        TableColumn<TableRelationship, String> childTable,
                                                        TableColumn<TableRelationship, String> type,
                                                        TableColumn<TableRelationship, String> parentTableColumn,
                                                        TableColumn<TableRelationship, String> childTableColumn) {
        parentTable.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getParentTable().getName()));
        childTable.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getChildTable().getName()));
        type.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getType().toString()));
        parentTableColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getParentTableColumn().getName()));
        childTableColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getChildTableColumn().getName()));
    }

    public static void setContextMenuTableRelation(TableView<TableRelationship> tableViewRelation, List<Table> tables, Database database) {
        ContextMenu contextMenu = new ContextMenu();
        MenuItem add = new MenuItem("Add");
        MenuItem edit = new MenuItem("Edit");
        MenuItem remove = new MenuItem("Remove");

        add.setOnAction(e -> {
            if (tables.size() < 2) {
                CustomAlerts.warning("Not enough tables", "Add at least 2 tables");
                return;
            }
            try {
                FXMLLoader loader = new FXMLLoader(SqlWorkbenchControllerMethods.class.getResource("/com/tiger/it/" +
                        "solutions/migration/tool/views/TableRelationshipView.fxml"));
                AnchorPane pane = loader.load();
                TableRelationshipController controller = loader.getController();
                controller.setTables(tables);
                controller.setTableRelationAction(TableRelationshipController.TableRelationAction.ADD);
                loadStageAndWait(pane, tableViewRelation.getScene().getWindow());
                if (controller.getTableRelationship() != null) {
                    tableViewRelation.getItems().add(controller.getTableRelationship());
                    tableViewRelation.refresh();
                    //database.addTableRelationship(controller.getTableRelationship());
                }
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
            }
        });

        contextMenu.getItems().addAll(add, edit, remove);
        tableViewRelation.setContextMenu(contextMenu);
    }

    public static void onTablesChanged(ObservableList<Table> tables, Database database) {
        tables.addListener((ListChangeListener<Table>) c -> {
            while (c.next()) {
                if (c.wasAdded())
                    c.getAddedSubList().forEach(database::addTable);
                if (c.wasUpdated()) {
                    int start = c.getFrom();
                    int end = c.getTo();
                    for (int i = start; i < end; i++) {
                        System.out.println("Updated" + c.getList().get(i).getName());
                    }
                }
                if (c.wasRemoved())
                    System.out.println("Removed");
                //c.getRemoved().forEach(database::removeTable);
            }
        });
    }

    public static void onTableRelationshipsChanged(ObservableList<TableRelationship> tableRelationships, Database database) {
        tableRelationships.addListener((ListChangeListener<TableRelationship>) c -> {
            while (c.next()) {
                if (c.wasAdded())
                    c.getAddedSubList().forEach(database::addTableRelationship);
            }
        });
    }

    public static void onTableChanged(TableView<Table> tableView, TableView<com.tiger.it.solutions.migration.tool.models.TableColumn> currentTableView) {
        tableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            currentTableView.getItems().clear();
            currentTableView.getItems().addAll(FXCollections.observableList(newValue.getColumns().stream().toList()));
        });
    }

    public static void setContextMenuTables(TableView<Table> tablesView, List<Table> tables) {
        ContextMenu contextMenu = new ContextMenu();
        MenuItem addTable = new MenuItem("Add");

        addTable.setOnAction(event -> {
            try {
                FXMLLoader loader = new FXMLLoader(SqlWorkbenchControllerMethods.class.getResource("/com/tiger/it/" +
                        "solutions/migration/tool/views/TableDialogView.fxml"));
                AnchorPane pane = loader.load();
                TableDialogController controller = loader.getController();
                controller.setTable(tablesView.getSelectionModel().getSelectedItem());
                controller.setTables(tables);
                controller.setEditType(TableDialogController.EditType.TABLE);
                Stage parentStage = (Stage) tablesView.getScene().getWindow();
                Scene scene = new Scene(pane);
                Stage stage = new Stage();
                stage.setScene(scene);
                stage.initOwner(parentStage);
                stage.initModality(Modality.NONE);
                stage.showAndWait();
                if (controller.getNewTable() != null) {
                    tables.add(controller.getNewTable());
                }
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
            }
        });

        contextMenu.getItems().add(addTable);
        tablesView.setContextMenu(contextMenu);
    }

    public static void setContextMenuCurrentTable(TableView<com.tiger.it.solutions.migration.tool.models.TableColumn> currentTable,
                                                  TableView<Table> tableView, Database database) {
        ContextMenu contextMenu = new ContextMenu();
        MenuItem add = new MenuItem("Add");
        MenuItem edit = new MenuItem("Edit");
        MenuItem remove = new MenuItem("Remove");

        add.setOnAction(e -> {
            if (tableView.getItems().isEmpty()) {
                CustomAlerts.warning("No tables specified", "Create a table before adding columns");
                return;
            }
            Table table = tableView.getSelectionModel().getSelectedItem();
            try {
                FXMLLoader loader = new FXMLLoader(SqlWorkbenchControllerMethods.class.getResource("/com/tiger/it/" +
                        "solutions/migration/tool/views/TableDialogView.fxml"));
                AnchorPane pane = loader.load();
                TableDialogController controller = loader.getController();
                controller.setEditType(TableDialogController.EditType.COLUMN);
                controller.setTableColumns(table.getColumns());
                loadStageAndWait(pane, currentTable.getScene().getWindow());
                if (controller.getNewColumn() != null) {
                    tableView.getSelectionModel().getSelectedItem().getColumns().add(controller.getNewColumn());
                    database.addTableColumn(table, controller.getNewColumn());
                }
                tableView.refresh();
                currentTable.refresh();
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
            }
        });

        edit.setOnAction(e -> {
            if (tableView.getItems().isEmpty()) {
                CustomAlerts.warning("No tables specified", "Create a table before editing columns");
                return;
            }

            com.tiger.it.solutions.migration.tool.models.TableColumn column = currentTable.getSelectionModel().getSelectedItem();
            try {
                if (column.isPrimary()) {
                    CustomAlerts.warning("Cannot edit primary column", "The primary column cannot be edited");
                    return;
                }
                FXMLLoader loader = new FXMLLoader(SqlWorkbenchControllerMethods.class.getResource("/com/tiger/it/" +
                        "solutions/migration/tool/views/TableDialogView.fxml"));
                AnchorPane pane = loader.load();
                TableDialogController controller = loader.getController();
                controller.setEditType(TableDialogController.EditType.COLUMN_EDIT);
                controller.setTableColumn(column);
                loadStageAndWait(pane, currentTable.getScene().getWindow());
                if (controller.getNewColumn() != null) {
                    tableView.getSelectionModel().getSelectedItem().getColumns().add(controller.getNewColumn());
                    database.updateTableColumn(column, controller.getNewColumn());
                }
                tableView.refresh();
                currentTable.refresh();
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
            }
        });

        remove.setOnAction(e -> {
            if (tableView.getItems().isEmpty()) {
                CustomAlerts.warning("No tables specified", "Create a table before removing columns");
                return;
            }

            com.tiger.it.solutions.migration.tool.models.TableColumn column = currentTable.getSelectionModel().getSelectedItem();
            if (column.isPrimary()) {
                CustomAlerts.warning("Cannot remove column", "Primary column cannot be removed");
                return;
            }
            if (CustomAlerts.confirmation("Remove column", "This action cannot be reversed!",
                    Alert.AlertType.WARNING)) {
                tableView.getSelectionModel().getSelectedItem().getColumns().remove(column);
                database.removeTableColumn(column);
                tableView.refresh();
                currentTable.refresh();
            }
        });

        contextMenu.getItems().addAll(add, edit, remove);
        currentTable.setContextMenu(contextMenu);
    }

    public static void setCurrentTableColumnsProperties(
            TableColumn<com.tiger.it.solutions.migration.tool.models.TableColumn, String> name,
            TableColumn<com.tiger.it.solutions.migration.tool.models.TableColumn, String> sqlType,
            TableColumn<com.tiger.it.solutions.migration.tool.models.TableColumn, String> rustType,
            TableColumn<com.tiger.it.solutions.migration.tool.models.TableColumn, String> nullAllowed,
            TableColumn<com.tiger.it.solutions.migration.tool.models.TableColumn, String> primary) {
        name.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
        sqlType.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getSqlRustType().key));
        rustType.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getSqlRustType().value));
        nullAllowed.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().isNullAllowed())));
        primary.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().isPrimary())));
    }

    public static ObservableList<Table> getTablesFromDatabase(TableView<Table> tableView, Database database) {
        ObservableList<Table> temp = FXCollections.observableList(database.getTables());
        tableView.setItems(temp);
        return temp;
    }

    public static ObservableList<TableRelationship> getTableRelationships(TableView<TableRelationship> tableView, List<Table> tables,
                                                                          Database database) {
        ObservableList<TableRelationship> temp = FXCollections.observableList(database.getTableRelationships(tables));
        tableView.setItems(temp);
        return temp;
    }
}
