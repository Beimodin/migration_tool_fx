package com.tiger.it.solutions.migration.tool.views.main.moduletab;

import com.tiger.it.solutions.migration.tool.databasehandlers.Database;
import com.tiger.it.solutions.migration.tool.errors.MissingDataException;
import com.tiger.it.solutions.migration.tool.models.Module;
import com.tiger.it.solutions.migration.tool.utils.CustomAlerts;
import com.tiger.it.solutions.migration.tool.utils.Hasher;
import com.tiger.it.solutions.migration.tool.utils.JsonHandler;
import com.tiger.it.solutions.migration.tool.views.main.moduletab.moduleadddialog.ModuleAddDialogController;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.commons.compress.utils.FileNameUtils;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ModuleTabControllerMethods {

    private static final Logger LOGGER = Logger.getLogger(ModuleTabControllerMethods.class.getName());

    private ModuleTabControllerMethods() {
    }

    public static void setTableViewProperties(TableView<Module> tableView) {
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tableView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
    }

    public static void setTableColumnProperties(TableColumn<Module, String> id, TableColumn<Module, String> name,
                                                TableColumn<Module, String> type, TableColumn<Module, String> description,
                                                TableColumn<Module, String> enable, TableColumn<Module, String> requiredBy) {
        id.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().id));
        name.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
        type.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getType().name()));
        description.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().limitDescription()));
        enable.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().isEnable())));
        requiredBy.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getRequiredByWithSeparator()));
    }

    public static void setContextMenu(TableView<Module> tableView, Database database) {
        ContextMenu contextMenu = new ContextMenu();
        MenuItem add = new MenuItem("Add");
        MenuItem addMultiple = new MenuItem("Add multiple");

        add.setOnAction(e -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Shell file (*.sh)", "*.sh"),
                    new FileChooser.ExtensionFilter("Powershell file (*.ps1)", "*.ps1"),
                    new FileChooser.ExtensionFilter("Module or Library file (*.rs)", "*.rs"));
            File file = fileChooser.showOpenDialog(tableView.getScene().getWindow());
            if (file == null) {
                return;
            }
            // Open dialog for description and requiredBy input
            String fileName = file.getName();
            if (tableView.getItems().stream().noneMatch(ex -> ex.getName().equals(fileName))) {
                String extension = FileNameUtils.getExtension(file.getPath());
                Module.ModuleType type = Module.getTypeFromFileExtension(fileName, extension);
                String content = Module.getContentByLength(file);

                Module module = new Module(Hasher.hash(fileName, Date.from(Instant.now()).toString()), fileName,
                        type, "", false, new ArrayList<>(), Module.getPathByExtension(type), content);
                try {
                    FXMLLoader loader = new FXMLLoader(ModuleTabControllerMethods.class.getResource(""));
                    AnchorPane pane = loader.load();
                    ModuleAddDialogController controller = loader.getController();
                    controller.setModule(module);
                    controller.setDatabase(database);
                    Stage parentStage = (Stage) tableView.getScene().getWindow();
                    Stage stage = new Stage();
                    Scene scene = new Scene(pane);
                    stage.setScene(scene);
                    stage.initOwner(parentStage);
                    stage.initModality(Modality.NONE);
                    stage.showAndWait();
                    if (controller.getModule() != null) {
                        database.addModule(module);
                        tableView.getItems().add(module);
                    }
                } catch (IOException ex) {
                    LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
                }
            }
        });

        addMultiple.setOnAction(e -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Modules json (*.json)", "*.json"));
            File file = fileChooser.showOpenDialog(tableView.getScene().getWindow());
            if (file == null) {
                return;
            }
            List<String> availableModules = new ArrayList<>();
            List<Module> modules = new ArrayList<>();
            database.getModules().forEach(module -> availableModules.add(module.getName()));
            try {
                modules = JsonHandler.createModules(file.getAbsolutePath(), availableModules);
            } catch (MissingDataException ex) {
                CustomAlerts.warning("Failed to add modules", ex.getMessage());
            } catch (IOException ex) {
                LOGGER.warning(ex.getMessage());
                CustomAlerts.warning("Failed to add module", ex.getMessage());
            }
            StringBuilder builder = new StringBuilder();
            builder.append("Following modules were not added (Already exist): ");
            modules.forEach(module -> {
                if (tableView.getItems().contains(module)) {
                    builder.append(module.getName()).append(", ");
                } else {
                    tableView.getItems().add(module);
                    database.addModule(module);
                }
            });
            if (builder.toString().contains(",")) {
                CustomAlerts.warning("Modules added with warning", builder.deleteCharAt(builder.length() - 1).toString());
            }
        });

        contextMenu.getItems().addAll(add, addMultiple);
        tableView.setContextMenu(contextMenu);
    }

    public static void onTableChange(TableView<Module> tableView, Database database) {
        tableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            database.updateModule(oldValue, newValue);
            tableView.refresh();
        });
    }

    public static void onTableColumnDoubleClick(TableView<Module> tableView) {
        tableView.setRowFactory(tv -> {
            TableRow<Module> row = new TableRow<>();
            row.setOnMouseClicked(e -> {
                if (e.getClickCount() == 2 && (!row.isEmpty())) {
                    Module module = row.getItem();
                }
            });
            return row;
        });
    }

    public static ObservableList<Module> createList(TableView<Module> tableView, Database database) {
        ObservableList<Module> temp = FXCollections.observableList(database.getModules());
        tableView.setItems(temp);
        return temp;
    }

    public static void searchModule(TextField textField, TableView<Module> tableView, ObservableList<Module> modules) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isEmpty()) {
                tableView.getItems().clear();
                tableView.getItems().addAll(modules);
                return;
            }
            tableView.getItems().clear();
            tableView.getItems().addAll(modules.stream().filter(e -> e.getName().contains(newValue)).toList());
        });
    }
}
