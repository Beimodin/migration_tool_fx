package com.tiger.it.solutions.migration.tool.views.main.menuitems.apitools.workflow.newworkflow;

import com.tiger.it.solutions.migration.tool.databasehandlers.Database;
import com.tiger.it.solutions.migration.tool.models.*;
import com.tiger.it.solutions.migration.tool.utils.HttpHandler;
import com.tiger.it.solutions.migration.tool.views.main.menuitems.apitools.workflow.newworkflow.workflowfilestructure.WorkflowFileStructureController;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TableColumn;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.util.Callback;
import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import static com.tiger.it.solutions.migration.tool.views.ViewLoader.loadStageAndWait;

public class NewApiWorkflowMethods {

    private NewApiWorkflowMethods() {
    }

    public static void setTableViewConstraints(TableView tableView) {
        tableView.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
    }

    public static ObservableList<String> fillApis(Database database) {
        return FXCollections.observableList(database.getApis().stream().map(Api::getName).collect(Collectors.toList()));
    }

    public static ObservableList<String> fillApiTypes(Database database) {
        return FXCollections.observableList(database.getApiTypes().stream().map(ApiType::getName).collect(Collectors.toList()));
    }

    public static void setTableViewDoubleClick(TableView<WorkflowFile> tableView) {
        tableView.setRowFactory(tv -> {
            TableRow<WorkflowFile> row = new TableRow<>();
            row.setOnMouseClicked(e -> {
                if (e.getClickCount() == 2 && !row.isEmpty()) {
                    try {
                        FXMLLoader loader = new FXMLLoader(NewApiWorkflowMethods.class.getResource("/com/tiger/it/" +
                                "solutions/migration/tool/views/ApiWorkflowFileStructureView.fxml"));
                        AnchorPane pane = loader.load();
                        WorkflowFileStructureController controller = loader.getController();
                        controller.setWorkflowFile(row.getItem());
                        loadStageAndWait(pane, tableView.getScene().getWindow());
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            });
            return row;
        });
    }

    public static void setTableColumnsWorkflowStructureProperties(TableColumn<WorkflowFile, String> fileName,
                                                                  TableColumn<WorkflowFile, String> activateFields,
                                                                  TableColumn<WorkflowFile, String> ignoredFields,
                                                                  TableColumn<WorkflowFile, String> fileType,
                                                                  TableColumn<WorkflowFile, String> unitTests) {
        fileName.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFileName()));
        activateFields.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getActivateFields())));
        ignoredFields.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getIgnoredFields())));
        fileType.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFileType()));
        unitTests.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().isUnitTestsEnabled())));
    }

    public static void setTableColumnWorkflowSettingsProperties(TableColumn<WorkflowSetting, String> name,
                                                                TableColumn<WorkflowSetting, String> value,
                                                                TableColumn<WorkflowSetting, String> defaultValue,
                                                                TableColumn<WorkflowSetting, String> globalInherited,
                                                                TableColumn<WorkflowSetting, String> overwriteGlobal) {
        name.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
        value.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getValue().getValueAsString()));
        defaultValue.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getDefaultValue().getValueAsString()));

        Callback<TableColumn<WorkflowSetting, String>, TableCell<WorkflowSetting, String>> cellFactoryInheritGlobalSetting = new Callback<>() {
            @Override
            public TableCell<WorkflowSetting, String> call(TableColumn<WorkflowSetting, String> param) {
                return new TableCell<>() {
                    final CheckBox checkBox = new CheckBox();

                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!empty) {
                            checkBox.setText("Inherit global setting");
                            checkBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
                                int index = indexProperty().getValue() < 0 ? 0 : indexProperty().getValue();
                                WorkflowSetting currentItem = getTableView().getItems().get(index);
                                currentItem.setGlobalInherited(checkBox.isSelected());
                            });
                            setGraphic(checkBox);
                        }
                        setText(null);
                    }
                };
            }
        };
        globalInherited.setCellFactory(cellFactoryInheritGlobalSetting);

        Callback<TableColumn<WorkflowSetting, String>, TableCell<WorkflowSetting, String>> cellFactoryOverwriteGlobalSetting = new Callback<>() {
            @Override
            public TableCell<WorkflowSetting, String> call(TableColumn<WorkflowSetting, String> param) {
                return new TableCell<>() {
                    final CheckBox checkBox = new CheckBox();

                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!empty) {
                            checkBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
                                int index = indexProperty().getValue() < 0 ? 0 : indexProperty().getValue();
                                WorkflowSetting currentItem = getTableView().getItems().get(index);
                                currentItem.setOverwriteGlobalSetting(checkBox.isSelected());
                            });
                            checkBox.setText("Overwrite global setting");
                        }
                        setText(null);
                    }
                };
            }
        };
        overwriteGlobal.setCellFactory(cellFactoryOverwriteGlobalSetting);
    }

    public static void preprocess(Button preProcess, Button moreInfo, Button structure, Button preview, Button previewHelp,
                                  Text msg, Config config, String file, ChoiceBox<String> fileType, Label fileCount) {
        if (!file.isEmpty()) {
            ExecutorService singleThread = Executors.newSingleThreadExecutor();

            preProcess.setOnAction(e -> {
                singleThread.execute(() -> {
                    try {
                        String fileContent = IOUtils.toString(new FileReader(file));
                        String result = "sas";//HttpHandler.executeRequest(HttpHandler.HttpType.POST, config.getServerConfig().getUrl(),
                                //"preprocess-structure", fileContent);
                        if (result.startsWith("s")) {
                            setStatusAfterPreProcess(false, preview, moreInfo, previewHelp, msg, result);
                        } else {
                            setStatusAfterPreProcess(true, preview, moreInfo, previewHelp, msg, "");
                        }
                    } catch (IOException ex) {
                        msg.setText("Failed to preprocess file structure");
                    }
                });
            });
        }
    }

    private static void addStructureFieldsToTable(TableView<WorkflowStructure> tableView, String result) {

    }

    private static void setStatusAfterPreProcess(boolean preProcessSuccess, Button previewStructure, Button moreInfo,
                                                 Button previewHelp, Text msg, String errorMessage) {
        if (preProcessSuccess) {
            previewStructure.setDisable(false);
            previewHelp.setDisable(false);
            msg.setText("No errors");
        } else {
            previewStructure.setDisable(true);
            previewHelp.setDisable(true);
            msg.setText(errorMessage);
        }
        moreInfo.setDisable(false);
    }
}
