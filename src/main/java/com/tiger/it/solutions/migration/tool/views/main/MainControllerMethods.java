package com.tiger.it.solutions.migration.tool.views.main;

import com.tiger.it.solutions.migration.tool.databasehandlers.Database;
import com.tiger.it.solutions.migration.tool.views.main.moduletab.ModuleTabController;
import com.tiger.it.solutions.migration.tool.views.main.sqlworkbench.SqlWorkbenchController;
import com.tiger.it.solutions.migration.tool.views.main.workflowtab.WorkflowTabController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainControllerMethods {

    private static final Logger LOGGER = Logger.getLogger(MainControllerMethods.class.getName());

    private MainControllerMethods() {
    }

    public static void onTabChange(AnchorPane mainPane, TabPane tabPane, MenuBar menuBar) {
        tabPane.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {

            // Load new menus and children from new selected tab
            AnchorPane pane = (AnchorPane) newValue.getContent();
            Optional<Node> bar = pane.getChildren().stream().filter(child -> child instanceof MenuBar).findFirst();

            /*if (bar.isPresent()) {
                menuBar.getMenus().clear();
                MenuBar tempMenuBar = (MenuBar) bar.get();
                menuBar.getMenus().addAll(tempMenuBar.getMenus());
            }*/
        });
    }

    public static void OnMenubarChange(MenuBar menuBar) {

    }

    public static void loadSqlWorkbench(Tab tab, Database database) {
        try {
            FXMLLoader loader = new FXMLLoader(MainControllerMethods.class.getResource("/com/tiger/it/solutions/migration/" +
                    "tool/views/SqlWorkbenchView.fxml"));
            AnchorPane pane = loader.load();
            SqlWorkbenchController controller = loader.getController();
            controller.setDatabase(database);
            tab.setContent(pane);
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    public static void loadModuleTab(Tab tab, Database database) {
        try {
            FXMLLoader loader = new FXMLLoader(MainControllerMethods.class.getResource("/com/tiger/it/solutions/migration/" +
                    "tool/views/ModuleTabView.fxml"));
            AnchorPane pane = loader.load();
            ModuleTabController controller = loader.getController();
            controller.setDatabase(database);
            tab.setContent(pane);
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, String.format("Failed to load module tab %s", ex.getMessage()), ex);
        }
    }

    public static void loadWorkflowTab(Tab tab, Database database) {
        try {
            FXMLLoader loader = new FXMLLoader(MainControllerMethods.class.getResource("/com/tiger/it/solutions/migration/" +
                    "tool/views/WorkflowView.fxml"));
            AnchorPane pane = loader.load();
            WorkflowTabController controller = loader.getController();
            controller.setDatabase(database);
            tab.setContent(pane);
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }
}
