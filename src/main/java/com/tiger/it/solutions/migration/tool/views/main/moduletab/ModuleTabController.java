package com.tiger.it.solutions.migration.tool.views.main.moduletab;

import com.tiger.it.solutions.migration.tool.databasehandlers.Database;
import com.tiger.it.solutions.migration.tool.models.Module;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class ModuleTabController implements Initializable {

    @FXML
    private TableView<Module> tableViewModules;

    @FXML
    private TableColumn<Module, String> tableColumnModuleId;

    @FXML
    private TableColumn<Module, String> tableColumnModuleName;

    @FXML
    private TableColumn<Module, String> tableColumnModuleType;

    @FXML
    private TableColumn<Module, String> tableColumnModuleDescription;

    @FXML
    private TableColumn<Module, String> tableColumnModuleEnable;

    @FXML
    private TableColumn<Module, String> tableColumnModuleRequiredBy;

    @FXML
    private TextField textFieldSearchModules;

    @FXML
    private Database database;

    private ObservableList<Module> modules;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ModuleTabControllerMethods.setTableViewProperties(tableViewModules);
        ModuleTabControllerMethods.setTableColumnProperties(tableColumnModuleId, tableColumnModuleName, tableColumnModuleType,
                tableColumnModuleDescription, tableColumnModuleEnable, tableColumnModuleRequiredBy);
        ModuleTabControllerMethods.onTableColumnDoubleClick(tableViewModules);

        Platform.runLater(() -> {
            modules = ModuleTabControllerMethods.createList(tableViewModules, database);
            ModuleTabControllerMethods.onTableChange(tableViewModules, database);
            ModuleTabControllerMethods.searchModule(textFieldSearchModules, tableViewModules, modules);
            ModuleTabControllerMethods.setContextMenu(tableViewModules, database);
        });
    }

    public void setDatabase(Database database) {
        this.database = database;
    }
}
