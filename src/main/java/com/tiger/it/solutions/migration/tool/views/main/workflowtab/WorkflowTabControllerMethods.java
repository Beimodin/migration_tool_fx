package com.tiger.it.solutions.migration.tool.views.main.workflowtab;

import com.tiger.it.solutions.migration.tool.databasehandlers.Database;
import com.tiger.it.solutions.migration.tool.models.Action;
import com.tiger.it.solutions.migration.tool.models.Workflow;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import java.util.List;

public class WorkflowTabControllerMethods {

    private WorkflowTabControllerMethods() {
    }

    public static ObservableList<String> loadNodeActions(ListView<String> listView, Database database) {
        List<Action> temp = database.getActions();
        ObservableList<String> actions = FXCollections.observableList(temp.stream().map(Action::getType).toList());
        listView.setItems(actions);
        return actions;
    }

    public static ObservableList<Workflow> loadAvailableWorkflows(ListView<Workflow> listView, Database database) {
        ObservableList<Workflow> workflows = FXCollections.observableList(database.getWorkflows());
        listView.setItems(workflows);
        return workflows;
    }

    public static void setListViewWorkflowsListener(ListView<Workflow> listViewWorkflows, ListView<String> listViewActions) {
        listViewWorkflows.getSelectionModel().selectedIndexProperty().addListener(((observable, oldValue, newValue) ->
                listViewActions.setDisable(newValue.intValue() == -1)));
    }

    public static void setActionListViewProperty(ListView<String> listView, VBox vBox) {
        listView.setCellFactory(new Callback<>() {
            @Override
            public ListCell<String> call(ListView<String> param) {
                return new ListCell<>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!empty) {
                            setText(item);
                            setOnMouseClicked(event -> {
                                if (event.getButton().equals(MouseButton.PRIMARY) && event.getClickCount() == 2) {
                                    vBox.getChildren().add(new TextArea("Node"));
                                }
                            });
                        }
                    }
                };
            }
        });
    }

    public static void setWorkflowListViewProperty(ListView<Workflow> listView) {
        listView.setCellFactory(new Callback<>() {
            @Override
            public ListCell<Workflow> call(ListView<Workflow> param) {
                return new ListCell<>() {
                    @Override
                    protected void updateItem(Workflow item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!empty) {
                            int currentIndex = indexProperty().getValue() < 0 ? 0 : indexProperty().getValue();
                            Workflow workflow = getListView().getItems().get(currentIndex);
                            setText(workflow.name);
                        }
                    }
                };
            }
        });
    }
}
