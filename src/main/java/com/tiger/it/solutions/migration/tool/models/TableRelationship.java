package com.tiger.it.solutions.migration.tool.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TableRelationship implements ModuleParser<TableRelationship> {

    private static final Logger LOGGER = Logger.getLogger(TableRelationship.class.getName());
    private RelationshipType type;
    private Table parentTable;
    private Table childTable;
    private TableColumn parentTableColumn;
    private TableColumn childTableColumn;

    public TableRelationship() {}

    public TableRelationship(RelationshipType type, Table parentTable, Table childTable, TableColumn parentTableColumn,
                             TableColumn childTableColumn) {
        this.type = type;
        this.parentTable = parentTable;
        this.childTable = childTable;
        this.parentTableColumn = parentTableColumn;
        this.childTableColumn = childTableColumn;
    }

    public RelationshipType getType() {
        return type;
    }

    public Table getParentTable() {
        return parentTable;
    }

    public Table getChildTable() {
        return childTable;
    }

    public TableColumn getParentTableColumn() {
        return parentTableColumn;
    }

    public TableColumn getChildTableColumn() {
        return childTableColumn;
    }

    @Override
    public List<TableRelationship> parse(String json) {
        List<TableRelationship> tableRelationships = new ArrayList<>();

        try {
            JSONObject root = new JSONObject(json);
            JSONArray jsonTableRelationships = root.getJSONArray("output");

            for (int i = 0; i < jsonTableRelationships.length(); ++i) {
                JSONObject temp = jsonTableRelationships.getJSONObject(i);
                Table parent = new Table().parse(temp.getString("table_parent")).get(0);
                Table child = new Table().parse(temp.getString("table_child")).get(0);
                TableColumn columnParent = new TableColumn().parse(temp.getString("table_column_parent")).get(0);
                TableColumn columnChild = new TableColumn().parse(temp.getString("table_column_child")).get(0);

                tableRelationships.add(new TableRelationship(RelationshipType.valueOf(temp.getString("relationship_type")),
                        parent, child, columnParent, columnChild));
            }
        } catch (JSONException ex) {
            LOGGER.log(Level.INFO, ex.getMessage(), ex);
            return new ArrayList<>();
        }

        return tableRelationships;
    }

    @Override
    public String toJson() {
        return null;
    }

    @Override
    public String listToJson(List<TableRelationship> objects, boolean feedback) {
        return null;
    }

    public enum RelationshipType {
        O2O, O2N, N2M
    }
}
