package com.tiger.it.solutions.migration.tool.utils;

import com.tiger.it.solutions.migration.tool.errors.MissingDataException;
import com.tiger.it.solutions.migration.tool.models.Module;
import com.tiger.it.solutions.migration.tool.models.Pair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class JsonHandler {
    private JsonHandler() {
    }

    public static List<Module> createModules(String file, List<String> availableModules) throws IOException {
        String content = Files.readString(Path.of(file), StandardCharsets.UTF_8);
        List<Module> modules = new ArrayList<>();
        List<String> allRequiredModules = new ArrayList<>();

        JSONObject root = new JSONObject(content);
        JSONArray mods = root.getJSONArray("modules");

        for (int i = 0; i < mods.length(); ++i) {
            JSONObject currentObj = mods.getJSONObject(i);
            String name = currentObj.getString("name");
            String description = currentObj.getString("description");
            File osPath = new File(currentObj.getString("path"));
            Module.ModuleType type = Module.ModuleType.valueOf(currentObj.getString("type"));
            List<String> temp = new ArrayList<>();
            JSONArray requiredBy = currentObj.getJSONArray("required_by");
            String requiredModuleName;
            for (int j = 0; j < requiredBy.length(); ++j) {
                requiredModuleName = requiredBy.getString(j);
                temp.add(requiredModuleName);
                allRequiredModules.add(requiredModuleName);
            }
            modules.add(new Module(Hasher.hash(name, Date.from(Instant.now()).toString()), name, type, description, false,
                    temp, Module.getPathByExtension(type), Module.getContentByLength(osPath)));
        }
        allRequiredModules.removeAll(availableModules);
        availableModules.removeAll(allRequiredModules);
        if (!allRequiredModules.equals(availableModules)) {
            throw new MissingDataException("One or more required modules are not available");
        }
        return modules;
    }

    public static <T> String createRequestBody(Pair<String, T>... searchTerms) {
        JSONObject root = new JSONObject();
        JSONArray search = new JSONArray("search");

        for (Pair<String, T> pair : searchTerms) {
            JSONObject temp = new JSONObject();
            temp.put("key", pair.key);
            temp.put("value", pair.value);

            search.put(temp);
        }

        return root.toString();
    }
}
