package com.tiger.it.solutions.migration.tool.views.main.sqlworkbench.tablerelationship;

import com.tiger.it.solutions.migration.tool.models.Table;
import com.tiger.it.solutions.migration.tool.models.TableRelationship;
import com.tiger.it.solutions.migration.tool.utils.CustomAlerts;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class TableRelationshipController implements Initializable {

    @FXML
    private ChoiceBox<TableRelationship.RelationshipType> choiceBoxRelationType;

    @FXML
    private ChoiceBox<String> choiceBoxParentTable;

    @FXML
    private ChoiceBox<String> choiceBoxChildTable;

    @FXML
    private TableView<com.tiger.it.solutions.migration.tool.models.TableColumn> tableViewParentColumn;

    @FXML
    private TableColumn<com.tiger.it.solutions.migration.tool.models.TableColumn, String> tableColumnParentColumnName;

    @FXML
    private TableColumn<com.tiger.it.solutions.migration.tool.models.TableColumn, String> tableColumnParentColumnSqlType;

    @FXML
    private TableColumn<com.tiger.it.solutions.migration.tool.models.TableColumn, String> tableColumnParentColumnRustType;

    @FXML
    private TableView<com.tiger.it.solutions.migration.tool.models.TableColumn> tableViewChildColumn;

    @FXML
    private TableColumn<com.tiger.it.solutions.migration.tool.models.TableColumn, String> tableColumnChildColumnName;

    @FXML
    private TableColumn<com.tiger.it.solutions.migration.tool.models.TableColumn, String> tableColumnChildColumnSqlType;

    @FXML
    private TableColumn<com.tiger.it.solutions.migration.tool.models.TableColumn, String> tableColumnChildColumnRustType;

    @FXML
    private Button buttonSave;

    @FXML
    private Button buttonCancel;

    @FXML
    private List<Table> tables;

    @FXML
    private TableRelationAction tableRelationAction;

    private TableRelationship tableRelationship;
    private TableRelationship newTableRelationship;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        TableRelationshipControllerMethods.setTableViewPolicy(tableViewParentColumn);
        TableRelationshipControllerMethods.setTableViewPolicy(tableViewChildColumn);
        TableRelationshipControllerMethods.setTableColumnParentProperties(tableColumnParentColumnName, tableColumnParentColumnSqlType,
                tableColumnParentColumnRustType);
        TableRelationshipControllerMethods.setTableColumnChildProperties(tableColumnChildColumnName, tableColumnChildColumnSqlType,
                tableColumnChildColumnRustType);
        TableRelationshipControllerMethods.setRelationTypes(choiceBoxRelationType);

        Platform.runLater(() -> {
            TableRelationshipControllerMethods.setTables(choiceBoxParentTable, tables);
            TableRelationshipControllerMethods.setTables(choiceBoxChildTable, tables);
            TableRelationshipControllerMethods.onTableColumnChange(choiceBoxParentTable, tableViewParentColumn, tables);
            TableRelationshipControllerMethods.onTableColumnChange(choiceBoxChildTable, tableViewChildColumn, tables);

            switch (tableRelationAction) {
                case ADD -> {
                }
                case EDIT -> {

                }
            }
            buttonSave.setOnAction(e -> {
                if (choiceBoxRelationType.getSelectionModel().getSelectedIndex() == -1) {
                    CustomAlerts.warning("No type selected", "Please selected a type");
                    return;
                }
                Optional<Table> parentTable = tables.stream().filter(et ->
                        et.getName().equals(choiceBoxParentTable.getSelectionModel().getSelectedItem())).findFirst();
                Optional<Table> childTable = tables.stream().filter(ec ->
                        ec.getName().equals(choiceBoxChildTable.getSelectionModel().getSelectedItem())).findFirst();
                if (parentTable.isEmpty() || childTable.isEmpty()) {
                    CustomAlerts.warning("Table not found", "At least one table was not found");
                    return;
                }
                newTableRelationship = new TableRelationship(choiceBoxRelationType.getSelectionModel().getSelectedItem(),
                        parentTable.get(), childTable.get(), tableViewParentColumn.getSelectionModel().getSelectedItem(),
                        tableViewChildColumn.getSelectionModel().getSelectedItem());
                if (newTableRelationship.getParentTableColumn().equals(newTableRelationship.getChildTableColumn())) {
                    CustomAlerts.warning("Cannot set relation", "Cannot set table relation between the same table");
                    return;
                } else if (TableRelationshipControllerMethods.onSave(newTableRelationship)) {
                    CustomAlerts.warning("Missing information", "Please fill in all information");
                    return;
                }
                tableRelationship = newTableRelationship;
                ((Stage) buttonSave.getScene().getWindow()).close();
            });
            buttonCancel.setOnAction(e -> ((Stage) buttonCancel.getScene().getWindow()).close());
        });
    }

    public void setTables(List<Table> tables) {
        this.tables = tables;
    }

    public void setTableRelationAction(TableRelationAction tableRelationAction) {
        this.tableRelationAction = tableRelationAction;
    }

    public TableRelationship getTableRelationship() {
        return tableRelationship;
    }

    public enum TableRelationAction {
        EDIT, ADD
    }
}
