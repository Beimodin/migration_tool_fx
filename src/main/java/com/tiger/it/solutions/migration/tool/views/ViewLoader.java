package com.tiger.it.solutions.migration.tool.views;

import com.tiger.it.solutions.migration.tool.databasehandlers.Database;
import com.tiger.it.solutions.migration.tool.models.Config;
import com.tiger.it.solutions.migration.tool.views.main.MainController;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ViewLoader {

    private static final Logger LOGGER = Logger.getLogger(ViewLoader.class.getName());

    private ViewLoader() {
    }

    public static void loadMainView(Stage stage, Database database) {
        database.startCheckSystem();
        try {
            FXMLLoader loader = new FXMLLoader(ViewLoader.class.getResource("/com/tiger/it/solutions/migration/tool/" +
                    "views/MainView.fxml"), ResourceBundle.getBundle("com.tiger.it.solutions.migration.tool.locales.TOOL",
                    Locale.getDefault()));
            AnchorPane pane = loader.load();
            MainController controller = loader.getController();
            controller.setDatabase(database);
            Scene scene = new Scene(pane);
            stage.setScene(scene);
            stage.setMinHeight(840);
            stage.setMinWidth(1270);
            stage.setResizable(true);
            stage.setTitle("Migration Tool");
            stage.setOnCloseRequest(event -> {
                LOGGER.info("Stopping databaseChecker timer");
                var test = database.stopCheckSystem();
                LOGGER.info("databaseChecker timer stopped");
                Platform.exit();
                System.exit(0);
            });
            stage.show();
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    public static void loadWaitingStage(List<Runnable> runnables) {
        try {
            FXMLLoader loader = new FXMLLoader(ViewLoader.class.getResource(""),
                    ResourceBundle.getBundle("com.tiger.it.solutions.migration.tool.locales.TOOL",
                            Locale.getDefault()));
            AnchorPane pane = loader.load();

        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    public static void loadStageAndWait(Pane pane, Window window) {
        Stage parentStage = (Stage) window;
        Scene scene = new Scene(pane);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.initOwner(parentStage);
        stage.initModality(Modality.NONE);
        stage.showAndWait();
    }
}
