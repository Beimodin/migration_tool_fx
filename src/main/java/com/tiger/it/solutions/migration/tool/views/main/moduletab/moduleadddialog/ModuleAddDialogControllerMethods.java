package com.tiger.it.solutions.migration.tool.views.main.moduletab.moduleadddialog;

import com.tiger.it.solutions.migration.tool.databasehandlers.Database;
import com.tiger.it.solutions.migration.tool.models.Module;
import javafx.collections.FXCollections;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

import java.util.List;

public class ModuleAddDialogControllerMethods {

    private ModuleAddDialogControllerMethods() {
    }

    public static void fillListView(ListView<Module> listView, Database database) {
        List<Module> modules = database.getModules();
        listView.setItems(FXCollections.observableList(modules));
    }

    public static void setListViewProperties(ListView<Module> listView) {
        listView.setCellFactory(new Callback<>() {
            @Override
            public ListCell<Module> call(ListView<Module> param) {
                return new ListCell<>() {
                    @Override
                    protected void updateItem(Module item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!empty) {
                            int currentIndex = indexProperty().getValue() < 0 ? 0 : indexProperty().getValue();
                            Module module = getListView().getItems().get(currentIndex);
                            setText(module.getName());
                        }
                    }
                };
            }
        });
    }

    public static boolean onSave(String name) {
        return name.isEmpty();
    }
}
