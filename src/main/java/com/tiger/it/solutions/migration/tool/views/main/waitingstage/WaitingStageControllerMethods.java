package com.tiger.it.solutions.migration.tool.views.main.waitingstage;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.util.ArrayList;
import java.util.List;

public class WaitingStageControllerMethods {

    private WaitingStageControllerMethods() {}

    public static void setTableViewConstraints(TableView tableView) {
        tableView.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
    }

    public static List<WaitingStageController.WaitingTask> createWaitingTasks(List<Runnable> runnables) {
        List<WaitingStageController.WaitingTask> temp = new ArrayList<>();

        runnables.forEach(runnable -> temp.add(new WaitingStageController.WaitingTask(String.valueOf(runnable.hashCode()),
                WaitingStageController.WaitingTask.WaitingTaskStatus.WAITING, false)));

        return temp;
    }

    public static void setTableColumnProperties(TableColumn<WaitingStageController.WaitingTask, String> name,
                                                TableColumn<WaitingStageController.WaitingTask, String> status,
                                                TableColumn<WaitingStageController.WaitingTask, Boolean> forceStop) {
        name.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
        status.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getStatus().toString()));
        forceStop.setCellValueFactory(cellData -> new SimpleBooleanProperty(cellData.getValue().isAllowForceStop()));

    }
}
