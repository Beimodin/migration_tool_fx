package com.tiger.it.solutions.migration.tool.models;

public class ApiType {

    public final String id;

    private String name;

    private boolean multiThreadAvailable;

    public ApiType(String id, String name, boolean multiThreadAvailable) {
        this.id = id;
        this.name = name;
        this.multiThreadAvailable = multiThreadAvailable;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isMultiThreadAvailable() {
        return multiThreadAvailable;
    }

    public void setMultiThreadAvailable(boolean multiThreadAvailable) {
        this.multiThreadAvailable = multiThreadAvailable;
    }
}
