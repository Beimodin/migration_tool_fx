package com.tiger.it.solutions.migration.tool.views.main.waitingstage;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

public class WaitingStageController implements Initializable {

    @FXML
    private ProgressBar progressBarWaitingTasksProgress;

    @FXML
    private Label labelWaitingTasksInformation;

    @FXML
    private TableView<WaitingTask> tableViewWaitingTasks;

    @FXML
    private TableColumn<WaitingTask, String> tableColumnWaitingTasksTaskName;

    @FXML
    private TableColumn<WaitingTask, String> tableColumnWaitingTasksTaskStatus;

    @FXML
    private TableColumn<WaitingTask, Boolean> tableColumnWaitingTasksTaskForceStop;

    private List<Runnable> runnables;
    private ObservableList<WaitingTask> waitingTasks;
    private HashMap<String, WaitingTask> stoppedTasks;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setRunnables(List<Runnable> runnables) {
        this.runnables = runnables;
    }

    public static class WaitingTask {

        private final String name;
        private WaitingTaskStatus status;

        private final boolean allowForceStop;

        public WaitingTask(String name, WaitingTaskStatus status, boolean allowForceStop) {
            this.name = name;
            this.status = status;
            this.allowForceStop = allowForceStop;
        }

        public String getName() {
            return name;
        }

        public WaitingTaskStatus getStatus() {
            return status;
        }

        public void setStatus(WaitingTaskStatus status) {
            this.status = status;
        }

        public boolean isAllowForceStop() {
            return allowForceStop;
        }

        public enum WaitingTaskStatus {
            WAITING,
            STOPPED,
            FAILED,
        }
    }
}
