package com.tiger.it.solutions.migration.tool.start;

import com.tiger.it.solutions.migration.tool.views.GUI;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        GUI.main(args);
    }
}
