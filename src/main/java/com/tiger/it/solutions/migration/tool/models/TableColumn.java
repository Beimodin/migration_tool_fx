package com.tiger.it.solutions.migration.tool.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Queue;

public class TableColumn implements ModuleParser<TableColumn> {

    private String name;
    private Pair<String, String> sqlRustType;
    private boolean isNullAllowed;
    private boolean isPrimary;

    public TableColumn() {}

    public TableColumn(String name, Pair<String, String> sqlRustType, boolean isNullAllowed, boolean isPrimary) {
        this.name = name;
        this.sqlRustType = sqlRustType;
        this.isNullAllowed = isNullAllowed;
        this.isPrimary = isPrimary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Pair<String, String> getSqlRustType() {
        return sqlRustType;
    }

    public void setSqlRustType(Pair<String, String> sqlRustType) {
        this.sqlRustType = sqlRustType;
    }

    public boolean isNullAllowed() {
        return isNullAllowed;
    }

    public void setNullAllowed(boolean nullAllowed) {
        isNullAllowed = nullAllowed;
    }

    public boolean isPrimary() {
        return isPrimary;
    }

    public void setPrimary(boolean primary) {
        isPrimary = primary;
    }

    public static boolean doesTableColumnNameAlreadyExists(Queue<TableColumn> columns, String tableColunnName) {
        Optional<TableColumn> op = columns.stream().filter(e -> e.name.equals(tableColunnName)).findAny();
        return op.isPresent();
    }

    @Override
    public List<TableColumn> parse(String json) {
        List<TableColumn> tableColumns = new ArrayList<>();

        JSONObject root = new JSONObject(json);
        JSONArray jsonTableColumns = root.getJSONArray("output");

        for (int i = 0; i < jsonTableColumns.length(); ++i) {
            JSONObject temp = jsonTableColumns.getJSONObject(i);
            tableColumns.add(new TableColumn(temp.getString("table_column_name"), new Pair<>(temp.getString("table_column_sql_type"),
                    temp.getString("table_column_rust_type")), temp.getBoolean("table_column_null_allowed"),
                    temp.getBoolean("table_column_primary_column")));
        }
        return tableColumns;
    }

    @Override
    public String toJson() {
        JSONObject object = new JSONObject();
        object.put("name", this.name);
        object.put("sql_type", this.sqlRustType.key);
        object.put("rust_type", this.sqlRustType.value);
        object.put("null_allowed", this.isNullAllowed);
        object.put("is_primary", this.isPrimary);
        return object.toString();
    }

    @Override
    public String listToJson(List<TableColumn> objects, boolean feedback) {
        return null;
    }
}
