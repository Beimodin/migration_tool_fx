package com.tiger.it.solutions.migration.tool.views.main.moduletab.moduleadddialog;

import com.tiger.it.solutions.migration.tool.databasehandlers.Database;
import com.tiger.it.solutions.migration.tool.models.Module;
import com.tiger.it.solutions.migration.tool.utils.CustomAlerts;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class ModuleAddDialogController implements Initializable {

    @FXML
    private TextField textFieldModuleName;

    @FXML
    private CheckBox checkBoxModuleEnabled;

    @FXML
    private ChoiceBox<Module.ModuleType> choiceBoxModuleType;

    @FXML
    private TextArea textAreaModuleDescription;

    @FXML
    private ListView<Module> listViewModuleRequiredBy;

    @FXML
    private Button buttonSave;

    @FXML
    private Button buttonCancel;

    @FXML
    private Database database;

    @FXML
    private Module module;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ModuleAddDialogControllerMethods.setListViewProperties(listViewModuleRequiredBy);

        buttonCancel.setOnAction(e -> ((Stage) buttonCancel.getScene().getWindow()).close());

        Platform.runLater(() -> {
            ModuleAddDialogControllerMethods.fillListView(listViewModuleRequiredBy, database);

            buttonSave.setOnAction(e -> {
                if (ModuleAddDialogControllerMethods.onSave(textFieldModuleName.getText())) {
                    CustomAlerts.warning("No name set", "A name has to be set");
                    return;
                }
                module.setDescription(textAreaModuleDescription.getText());
                module.setEnable(checkBoxModuleEnabled.isSelected());
                module.setType(choiceBoxModuleType.getSelectionModel().getSelectedItem());
                module.setName(textFieldModuleName.getText());
                ((Stage) buttonSave.getScene().getWindow()).close();
            });
        });
    }

    public void setDatabase(Database database) {
        this.database = database;
    }

    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }
}
