package com.tiger.it.solutions.migration.tool.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class Action implements ModuleParser<Action> {

    private static final Logger LOGGER = Logger.getLogger(Action.class.getName());

    private final String type;

    public Action() {
        this.type = "";
    }

    public Action(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    @Override
    public List<Action> parse(String json) {
        List<Action> actions = new ArrayList<>();

        try {
            JSONObject root = new JSONObject(json);
            JSONArray jsonActions = root.getJSONArray("output");

            for (int i = 0; i < jsonActions.length(); ++i) {
                actions.add(new Action(jsonActions.getString(i)));
            }
        } catch (JSONException ex) {
            LOGGER.warning(ex.getMessage());
        }

        return actions;
    }

    @Override
    public String toJson() {
        return null;
    }

    @Override
    public String listToJson(List<Action> objects, boolean feedback) {
        return null;
    }
}
