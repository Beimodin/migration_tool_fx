package com.tiger.it.solutions.migration.tool.models;

import java.util.List;

public class FileStructure {

    public final Structure startNode;

    public FileStructure(Structure startNode) {
        this.startNode = startNode;
    }

    public static class Structure {

        public final List<Structure> structuresOnSameLevel;

        public final Structure nextStructure;

        public Structure(List<Structure> structuresOnSameLevel, Structure nextStructure) {
            this.structuresOnSameLevel = structuresOnSameLevel;
            this.nextStructure = nextStructure;
        }
    }
}
