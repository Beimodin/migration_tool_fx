package com.tiger.it.solutions.migration.tool.views.main.menuitems.apitools.workflow.newworkflow.workflowfilestructure;

import com.tiger.it.solutions.migration.tool.models.WorkflowFile;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;

public class WorkflowFileStructureController implements Initializable {

    @FXML
    private WorkflowFile workflowFile;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setWorkflowFile(WorkflowFile workflowFile) {
        this.workflowFile = workflowFile;
    }
}
