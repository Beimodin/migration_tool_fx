package com.tiger.it.solutions.migration.tool.models;

import java.util.List;
import java.util.Queue;

public interface ModuleParser<T> {
    List<T> parse(String json);

    String toJson();
    String listToJson(List<T> objects, boolean feedback);
}
