package com.tiger.it.solutions.migration.tool.views.main.sqlworkbench;

import com.tiger.it.solutions.migration.tool.databasehandlers.Database;
import com.tiger.it.solutions.migration.tool.models.Table;
import com.tiger.it.solutions.migration.tool.models.TableRelationship;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.net.URL;
import java.util.ResourceBundle;

public class SqlWorkbenchController implements Initializable {

    @FXML
    private MenuBar menuBarSqlWorkBench;

    @FXML
    private TableView<Table> tableViewTables;

    @FXML
    private TableColumn<Table, String> tableColumnTablesId;

    @FXML
    private TableColumn<Table, String> tableColumnTablesName;

    @FXML
    private TableColumn<Table, String> tableColumnTablesPrimaryColumn;

    @FXML
    private TableColumn<Table, String> tableColumnTablesColumnSize;

    @FXML
    private TableColumn<Table, String> tableColumnTablesEdit;

    @FXML
    private TableColumn<Table, String> tableColumnTablesDelete;

    @FXML
    private TableView<TableRelationship> tableViewTableRelationship;

    @FXML
    private TableColumn<TableRelationship, String> tableColumnRelationshipParentTable;

    @FXML
    private TableColumn<TableRelationship, String> tableColumnRelationshipChildTable;

    @FXML
    private TableColumn<TableRelationship, String> tableColumnRelationshipType;

    @FXML
    private TableColumn<TableRelationship, String> tableColumnRelationshipParentTableColumn;

    @FXML
    private TableColumn<TableRelationship, String> tableColumnRelationshipChildTableColumn;

    @FXML
    private TableView<com.tiger.it.solutions.migration.tool.models.TableColumn> tableViewCurrentTable;

    @FXML
    private TableColumn<com.tiger.it.solutions.migration.tool.models.TableColumn, String> tableColumnCurrentTableId;

    @FXML
    private TableColumn<com.tiger.it.solutions.migration.tool.models.TableColumn, String> tableColumnCurrentTableColumnName;

    @FXML
    private TableColumn<com.tiger.it.solutions.migration.tool.models.TableColumn, String> tableColumnCurrentTableSqlType;

    @FXML
    private TableColumn<com.tiger.it.solutions.migration.tool.models.TableColumn, String> tableColumnCurrentTableRustType;

    @FXML
    private TableColumn<com.tiger.it.solutions.migration.tool.models.TableColumn, String> tableColumnCurrentTableNullAllowed;

    @FXML
    private TableColumn<com.tiger.it.solutions.migration.tool.models.TableColumn, String> tableColumnCurrentTablePrimaryKey;

    @FXML
    private Database database;

    private ObservableList<Table> tables;
    private ObservableList<TableRelationship> tableRelationships;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        SqlWorkbenchControllerMethods.setTableViewResizePolicy(tableViewCurrentTable);
        SqlWorkbenchControllerMethods.setTableViewResizePolicy(tableViewTables);
        SqlWorkbenchControllerMethods.setTableViewResizePolicy(tableViewTableRelationship);
        SqlWorkbenchControllerMethods.setTableRelationColumnProperties(tableColumnRelationshipParentTable,
                tableColumnRelationshipChildTable, tableColumnRelationshipType, tableColumnRelationshipParentTableColumn,
                tableColumnRelationshipChildTableColumn);

        Platform.runLater(() -> {
            SqlWorkbenchControllerMethods.setTableColumnsProperty(tableColumnTablesId, tableColumnTablesName, tableColumnTablesPrimaryColumn,
                    tableColumnTablesColumnSize, tableColumnTablesEdit, tableColumnTablesDelete, database);

            SqlWorkbenchControllerMethods.setCurrentTableColumnsProperties(tableColumnCurrentTableColumnName,
                    tableColumnCurrentTableSqlType, tableColumnCurrentTableRustType, tableColumnCurrentTableNullAllowed,
                    tableColumnCurrentTablePrimaryKey);

            tables = SqlWorkbenchControllerMethods.getTablesFromDatabase(tableViewTables, database);
            tableRelationships = SqlWorkbenchControllerMethods.getTableRelationships(tableViewTableRelationship, tables, database);
            SqlWorkbenchControllerMethods.onTablesChanged(tables, database);
            SqlWorkbenchControllerMethods.onTableRelationshipsChanged(tableRelationships, database);
            SqlWorkbenchControllerMethods.setContextMenuTables(tableViewTables, tables);
            SqlWorkbenchControllerMethods.setContextMenuCurrentTable(tableViewCurrentTable, tableViewTables, database);
            SqlWorkbenchControllerMethods.onTableChanged(tableViewTables, tableViewCurrentTable);
            SqlWorkbenchControllerMethods.setContextMenuTableRelation(tableViewTableRelationship, tables, database);
        });
    }

    public void setDatabase(Database database) {
        this.database = database;
    }
}
