# How-To: Add multiple modules at once via JSON

**_IMPORTANT: A manual review cannot be performed_**
**_Therefor all modules in a json file must be valid_**
**_If at least one module is in any way invalid, none of the modules will be added_**

## File structure

* root
    * modules array
        * module:
            * name (Module name)
            * description
            * path (On the OS. If possible provide the absolute path)
            * type (Moduletype: Valid types are: SCRIPT, TOOL, LIBRARY, MODULE)
            * required_by array (Module name)